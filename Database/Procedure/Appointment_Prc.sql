-- Randevu Durumu Değiştirme
@prc_name = prcChangeStateOfAppointment
@params = {app_id, detail_text, state_value, emp_id}
BEGIN
    INSERT INTO appointment_detail(AppointmentID, DetailText, StateValue, EmployeeID) VALUES (app_id, detail_text, state_value, emp_id);
    UPDATE appointment ap SET ap.State = state_value WHERE ap.ID = app_id;
END
-- Randevu Oluşturma
@prc_name = prcCreateAppointment
@params = {appointment_date, appointment_time, customer_id, employee_id, business_id}
BEGIN
	INSERT INTO appointment (AppointmentDate, AppointmentTime, CustomerID, EmployeeID, BusinessID) 
	VALUES (appointment_date, appointment_time, customer_id, employee_id, business_id);
	
	SELECT LAST_INSERT_ID() AS AppointmentID;
END

-- Randevu Tekrarlama
@prc_name = prcRepeatAppointment
@params = {recurring_id, appointment_time, appointment_date}
BEGIN
	
	DECLARE cus_id INT;
	DECLARE emp_id INT;
	DECLARE bus_id INT;

	SELECT ap.CustomerID, ap.EmployeeID, ap.BusinessID INTO cus_id, emp_id, bus_id FROM appointment ap WHERE ap.ID = recurring_id;
	CALL prcCreateAppointment(appointment_date, appointment_time, cus_id, emp_id, bus_id);
	SELECT LAST_INSERT_ID();
END