@prc_name = PrcCreateSubCategory
@params = {category_value}
BEGIN
  INSERT INTO businesssubcategory(Value, Description)
  VALUES (category_value, 'Yeni bir alt işlem birimi');

  SELECT LAST_INSERT_ID() AS LastSubCategoryID;
END

@prc_name = PrcCreateBusinessField
@params = {top_category_id, sub_category_id, price_value, bonus_ratio}
BEGIN
  DECLARE DescText TEXT;
  DECLARE TopCategoryValue VARCHAR(255);
  DECLARE SubCategoryValue VARCHAR(255);

  SELECT btc.Value INTO TopCategoryValue FROM businesstopcategory btc WHERE btc.ID = top_category_id;
  SELECT bsc.Value INTO SubCategoryValue FROM businesssubcategory bsc WHERE bsc.ID = sub_category_id;

  SET DescText = CONCAT(TopCategoryValue, ' <i class="fas fa-random"></i> ', SubCategoryValue);

  INSERT INTO businesstransaction(TopCategoryID, SubCategoryID, Pricing, Description, BonusRatio) VALUES (top_category_id, sub_category_id, price_value, DescText, bonus_ratio);
  SELECT LAST_INSERT_ID() AS LastBusinessFieldID;
END

@prc_name = PrcUpdateBusinessField
@params = {business_id, pricing, bonus_ratio}
BEGIN
  UPDATE businesstransaction bt SET bt.Pricing = pricing, bt.BonusRatio = bonus_ratio WHERE bt.ID = business_id;
END

@prc_name = PrcBusinessActiveChange
@params = {business_id, change_value}
BEGIN
  UPDATE businesstransaction bt SET bt.IsActive = change_value WHERE bt.ID = business_id;
END
