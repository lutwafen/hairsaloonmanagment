
-- Randevu Listesi
CREATE VIEW vw_appointmentlist AS
SELECT 
    ap.ID,
    ap.AppointmentDate,
    ap.AppointmentTime,
    CONCAT('<i class="fas fa-calendar-times"></i>', ap.AppointmentDate, ' - <i class="fas fa-clock"></i>', ap.AppointmentTime) AS FullDate,
    (
        CASE 
            WHEN ap.State = 0 THEN 'Müşteri Randevuya Bekleniyor'
            WHEN ap.State = 1 THEN 'Müşteri Randevuya Geldi'
            WHEN ap.State = 2 THEN 'Müşteri Randevuya Gelmedi'
            WHEN ap.State = 3 THEN 'Randevu İptal Edildi'
        END
    ) AS StateOfDescription,

    cus.Fullname,
    cus.Phone,
    bus.Description AS JobField,
    ap.EmployeeID,
    ap.State
FROM appointment ap
INNER JOIN customer cus ON cus.ID = ap.CustomerID
INNER JOIN Employee emp ON emp.ID = ap.EmployeeID
INNER JOIN BusinessTransaction bus ON bus.ID = ap.BusinessID 

-- Randevu Durum Detayı [Tüm Liste]
CREATE VIEW vw_stateofdetail AS
SELECT  ad.*, emp.Fullname
FROM appointment_detail ad 
INNER JOIN employee emp ON emp.ID = ad.EmployeeID;