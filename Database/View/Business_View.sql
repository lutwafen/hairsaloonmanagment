CREATE VIEW vw_businesslist
AS
SELECT bt.ID, bt.BonusRatio, bt.TopCategoryID, bt.SubCategoryID, btc.Value as TopCategoryValue, bsc.Value as SubCategoryValue, bt.Pricing, bt.Description, bt.NonCategory, bt.IsActive FROM businesstransaction bt
INNER JOIN businesstopcategory btc ON btc.ID = bt.TopCategoryID
INNER JOIN businesssubcategory bsc ON bsc.ID = bt.SubCategoryID;

CREATE VIEW vw_sub_business_list
AS
SELECT * FROM businesssubcategory;

CREATE VIEW vw_top_business_list
AS
SELECT * FROM businesstopcategory;
