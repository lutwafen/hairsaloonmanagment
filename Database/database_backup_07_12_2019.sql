-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 07 Ara 2019, 12:28:02
-- Sunucu sürümü: 10.4.8-MariaDB
-- PHP Sürümü: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `kuafor_app_demo`
--
CREATE DATABASE IF NOT EXISTS `kuafor_app_demo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `kuafor_app_demo`;

DELIMITER $$
--
-- Yordamlar
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `PrcBusinessActiveChange` (IN `business_id` INT, IN `change_value` TINYINT)  BEGIN
  UPDATE businesstransaction bt SET bt.IsActive = change_value WHERE bt.ID = business_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcChangeStateOfAppointment` (IN `app_id` INT, IN `detail_text` TEXT, IN `state_value` TINYINT, IN `emp_id` INT)  BEGIN
    INSERT INTO appointment_detail(AppointmentID, DetailText, StateValue, EmployeeID) VALUES (app_id, detail_text, state_value, emp_id);
    UPDATE appointment ap SET ap.State = state_value WHERE ap.ID = app_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcCreateAppointment` (IN `appointment_date` VARCHAR(50), IN `appointment_time` VARCHAR(50), IN `customer_id` INT, IN `employee_id` INT, IN `business_id` INT)  BEGIN
	INSERT INTO appointment (AppointmentDate, AppointmentTime, CustomerID, EmployeeID, BusinessID) 
	VALUES (appointment_date, appointment_time, customer_id, employee_id, business_id);
	
	SELECT LAST_INSERT_ID() AS AppointmentID;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PrcCreateBusinessField` (IN `top_category_id` INT, IN `sub_category_id` INT, IN `price_value` DECIMAL(15,2), IN `bonus_ratio` TINYINT)  BEGIN
  DECLARE DescText TEXT;
  DECLARE TopCategoryValue VARCHAR(255);
  DECLARE SubCategoryValue VARCHAR(255);

  SELECT btc.Value INTO TopCategoryValue FROM businesstopcategory btc WHERE btc.ID = top_category_id;
  SELECT bsc.Value INTO SubCategoryValue FROM businesssubcategory bsc WHERE bsc.ID = sub_category_id;

  SET DescText = CONCAT(TopCategoryValue, ' <i class="fas fa-random"></i> ', SubCategoryValue);

  INSERT INTO businesstransaction(TopCategoryID, SubCategoryID, Pricing, Description, BonusRatio) VALUES (top_category_id, sub_category_id, price_value, DescText, bonus_ratio);
  SELECT LAST_INSERT_ID() AS LastBusinessFieldID;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PrcCreateSubCategory` (IN `category_value` VARCHAR(255))  BEGIN
  INSERT INTO businesssubcategory(Value, Description)
  VALUES (category_value, 'Yeni bir alt işlem birimi');

  SELECT LAST_INSERT_ID() AS LastSubCategoryID;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcRepeatAppointment` (IN `recurring_id` INT, IN `appointment_time` VARCHAR(50), IN `appointment_date` VARCHAR(50))  BEGIN
	
	DECLARE cus_id INT;
	DECLARE emp_id INT;
	DECLARE bus_id INT;

	SELECT ap.CustomerID, ap.EmployeeID, ap.BusinessID INTO cus_id, emp_id, bus_id FROM appointment ap WHERE ap.ID = recurring_id;
	CALL prcCreateAppointment(appointment_date, appointment_time, cus_id, emp_id, bus_id);
	SELECT LAST_INSERT_ID();
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PrcUpdateBusinessField` (IN `business_id` INT, IN `pricing` DECIMAL(10,2), IN `bonus_ratio` TINYINT)  BEGIN
  UPDATE businesstransaction bt SET bt.Pricing = pricing, bt.BonusRatio = bonus_ratio WHERE bt.ID = business_id;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `appointment`
--

CREATE TABLE `appointment` (
  `ID` int(11) NOT NULL,
  `AppointmentDate` date DEFAULT NULL,
  `AppointmentTime` varchar(5) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `EmployeeID` int(11) DEFAULT NULL,
  `BusinessID` int(11) DEFAULT NULL,
  `State` tinyint(3) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `appointment`
--

INSERT INTO `appointment` (`ID`, `AppointmentDate`, `AppointmentTime`, `CustomerID`, `EmployeeID`, `BusinessID`, `State`) VALUES
(1, '2019-11-16', '07:30', 1, 1, 1, 0),
(2, '2019-11-27', '11:00', 2, 1, 1, 0),
(3, '2019-11-27', '11:00', 2, 1, 1, 0),
(4, '2019-11-27', '11:00', 2, 1, 1, 0),
(5, '2019-11-27', '11:00', 2, 1, 1, 0),
(6, '2019-11-27', '11:00', 2, 1, 1, 0),
(7, '2019-11-27', '11:00', 2, 1, 1, 0),
(8, '2019-11-27', '11:00', 2, 1, 1, 0),
(9, '2019-11-27', '11:00', 2, 1, 1, 0),
(10, '2019-11-27', '11:00', 2, 1, 1, 0),
(11, '2019-11-27', '11:00', 2, 1, 1, 0),
(12, '2019-11-27', '11:00', 2, 1, 1, 0),
(13, '2019-11-27', '11:00', 2, 1, 1, 0),
(14, '2019-11-27', '11:00', 2, 1, 1, 0),
(15, '2019-11-27', '11:00', 2, 1, 1, 0),
(23, '2019-11-27', '11:43', 1, 2, 1, 0),
(24, '2019-11-27', '11:43', 1, 2, 1, 0),
(25, '2019-11-27', '11:43', 1, 2, 1, 0),
(26, '2019-11-27', '11:43', 1, 2, 1, 0),
(27, '2019-11-27', '11:43', 1, 2, 1, 1),
(28, '2019-11-27', '11:43', 1, 2, 1, 1),
(29, '2019-11-27', '11:43', 1, 2, 1, 0),
(30, '2019-11-27', '11:43', 1, 2, 1, 0),
(31, '2019-11-27', '11:43', 1, 2, 1, 0),
(32, '2019-11-27', '11:43', 1, 2, 1, 0),
(35, '2019-11-27', '11:48', 3, 1, 1, 1),
(36, '2019-11-27', '11:48', 3, 1, 1, 2),
(37, '2019-11-27', '11:48', 3, 1, 1, 3),
(48, '2019-11-28', '18:47', 3, 1, 1, 1),
(49, '2019-11-28', '18:50', 2, 1, 1, 1),
(50, '2019-11-30', '11:11', 2, 1, 1, 0),
(51, '2019-11-28', '00:14', 1, 2, 1, 0),
(52, '2019-11-28', '00:14', 1, 2, 1, 0),
(53, '2019-11-28', '00:15', 1, 2, 1, 0),
(54, '2019-11-29', '07:35', 3, 1, 1, 2),
(55, '2019-11-29', '21:27', 1, 1, 1, 1),
(56, '2019-11-29', '21:47', 1, 1, 1, 1),
(57, '2019-12-02', '19:24', 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `appointment_detail`
--

CREATE TABLE `appointment_detail` (
  `ID` int(11) NOT NULL,
  `AppointmentID` int(11) NOT NULL,
  `Fulldate` datetime NOT NULL DEFAULT current_timestamp(),
  `DetailText` text NOT NULL,
  `StateValue` tinyint(4) NOT NULL,
  `EmployeeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `appointment_detail`
--

INSERT INTO `appointment_detail` (`ID`, `AppointmentID`, `Fulldate`, `DetailText`, `StateValue`, `EmployeeID`) VALUES
(4, 37, '2019-11-28 12:00:39', 'test', 3, 1),
(5, 36, '2019-11-28 12:01:39', 'Müşteri randevuya gelmedi. Bilgi Durumu : hayır', 2, 1),
(6, 35, '2019-11-28 12:01:51', 'Müşteri randevuya geldi, randevu işlemi başlatıldı!', 1, 2),
(7, 48, '2019-11-28 20:48:43', 'Müşteri randevuya geldi, randevu işlemi başlatıldı!', 1, 1),
(8, 49, '2019-11-28 20:51:01', 'Müşteri randevuya geldi, randevu işlemi başlatıldı!', 1, 2),
(9, 28, '2019-11-29 00:14:27', 'Müşteri randevuya geldi, randevu işlemi başlatıldı!', 1, 1),
(10, 27, '2019-11-29 00:15:40', 'Müşteri randevuya geldi, randevu işlemi başlatıldı!', 1, 1),
(11, 54, '2019-11-29 23:27:06', 'Müşteri randevuya gelmedi. Bilgi Durumu : Evet', 2, 1),
(12, 55, '2019-11-29 23:28:45', 'Müşteri randevuya geldi, randevu işlemi başlatıldı!', 1, 1),
(13, 56, '2019-11-29 23:47:55', 'Müşteri randevuya geldi, randevu işlemi başlatıldı!', 1, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `businesssubcategory`
--

CREATE TABLE `businesssubcategory` (
  `ID` int(11) NOT NULL,
  `Value` varchar(50) NOT NULL,
  `Description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `businesssubcategory`
--

INSERT INTO `businesssubcategory` (`ID`, `Value`, `Description`) VALUES
(1, 'Bacak', 'Ağda Kategorisinin Bir Alt Kategorisi'),
(22, 'Sırt', 'Yeni bir alt işlem birimi'),
(23, 'Kol', 'Yeni bir alt işlem birimi'),
(24, 'Kol 2', 'Yeni bir alt işlem birimi'),
(25, 'Kol Altı', 'Yeni bir alt işlem birimi');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `businesstopcategory`
--

CREATE TABLE `businesstopcategory` (
  `ID` int(11) NOT NULL,
  `Value` varchar(50) NOT NULL,
  `Description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `businesstopcategory`
--

INSERT INTO `businesstopcategory` (`ID`, `Value`, `Description`) VALUES
(1, 'Ağda', 'Genel Bir Kategoridir');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `businesstransaction`
--

CREATE TABLE `businesstransaction` (
  `ID` int(11) NOT NULL,
  `TopCategoryID` int(11) DEFAULT NULL,
  `SubCategoryID` int(11) DEFAULT NULL,
  `Pricing` decimal(15,2) DEFAULT 0.00,
  `Description` varchar(120) DEFAULT NULL,
  `BonusRatio` tinyint(4) NOT NULL DEFAULT 0,
  `NonCategory` tinyint(4) DEFAULT 0,
  `IsActive` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `businesstransaction`
--

INSERT INTO `businesstransaction` (`ID`, `TopCategoryID`, `SubCategoryID`, `Pricing`, `Description`, `BonusRatio`, `NonCategory`, `IsActive`) VALUES
(1, 1, 1, '32.00', 'Ağda <i class=\"fas fa-random\"></i> Bacak', 4, 0, 1),
(2, 1, 2, '20.00', 'Ağda <i class=\"fas fa-random\"></i> gene', 8, 0, 1),
(3, 1, 22, '18.00', 'Ağda <i class=\"fas fa-random\"></i> Sırt', 8, 0, 1),
(4, 1, 23, '0.00', 'Ağda <i class=\"fas fa-random\"></i> Kol', 10, 0, 1),
(5, 1, 24, '250.00', 'Ağda <i class=\"fas fa-random\"></i> Kol 2', 10, 0, 1),
(6, 1, 25, '75.00', 'Ağda <i class=\"fas fa-random\"></i> Kol Altı', 18, 0, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `customer`
--

CREATE TABLE `customer` (
  `ID` int(11) NOT NULL,
  `Fullname` varchar(32) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `Tag` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `customer`
--

INSERT INTO `customer` (`ID`, `Fullname`, `Phone`, `Tag`) VALUES
(1, 'Burcu Babacan', ' 0 535 684 98 39', 'burcu-babacan'),
(2, 'İrem Kunt', '0 (535) 442 20 33', 'irem-kunt'),
(3, 'Ümran Türkdoğan', '+90 (508) 535 48 27', 'umran-turkdogan');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `customersafedata`
--

CREATE TABLE `customersafedata` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `TotalPrice` decimal(15,2) DEFAULT NULL,
  `SafeID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `customersafemovementinfo`
--

CREATE TABLE `customersafemovementinfo` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `TransactionID` int(11) DEFAULT NULL,
  `TransactionPrice` decimal(15,2) DEFAULT NULL,
  `EmployeeID` int(11) DEFAULT NULL,
  `SafeID` int(11) DEFAULT NULL,
  `SafeMovementID` int(11) DEFAULT NULL,
  `CreatedAt` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `employee`
--

CREATE TABLE `employee` (
  `ID` int(11) NOT NULL,
  `Fullname` varchar(50) DEFAULT NULL,
  `Username` varchar(16) DEFAULT NULL,
  `Password` varchar(16) DEFAULT NULL,
  `Address` varchar(100) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT NULL,
  `IsAdmin` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `employee`
--

INSERT INTO `employee` (`ID`, `Fullname`, `Username`, `Password`, `Address`, `Phone`, `IsActive`, `IsAdmin`) VALUES
(1, 'Fatih Çalışan', 'fatih', '1234', 'Lorem Ipsum Sit Dolor', '0000', 1, 1),
(2, 'Burcu Beşok', ' burcsok', '1234', 'Canberk Sokak 748 08215 Uşak', '+90 530 890 81 8', 1, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `employeebonus`
--

CREATE TABLE `employeebonus` (
  `ID` int(11) NOT NULL,
  `EmployeeID` int(11) NOT NULL,
  `BusinessID` int(11) NOT NULL,
  `BonusAmount` decimal(10,2) NOT NULL,
  `SafeMovementPrice` decimal(10,2) NOT NULL,
  `Date` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `employeelog`
--

CREATE TABLE `employeelog` (
  `ID` int(11) NOT NULL,
  `EmployeeID` int(11) DEFAULT NULL,
  `LogReg` text DEFAULT NULL,
  `LogDate` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `groupcustomer`
--

CREATE TABLE `groupcustomer` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `GroupID` int(11) DEFAULT NULL,
  `MovementCount` int(11) DEFAULT NULL,
  `PricingStatus` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `groups`
--

CREATE TABLE `groups` (
  `ID` int(11) NOT NULL,
  `CreatedCustomerID` int(11) DEFAULT NULL,
  `SafeID` int(11) DEFAULT NULL,
  `NumberOfPeople` int(11) DEFAULT NULL,
  `Date` datetime(3) DEFAULT NULL,
  `Tag` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `groupsafemovement`
--

CREATE TABLE `groupsafemovement` (
  `ID` int(11) NOT NULL,
  `SafeMovementID` int(11) DEFAULT NULL,
  `SafeID` int(11) DEFAULT NULL,
  `GCustomerID` int(11) DEFAULT NULL,
  `GroupID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `material`
--

CREATE TABLE `material` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `CategoryID` int(11) DEFAULT NULL,
  `Price` decimal(15,2) DEFAULT NULL,
  `UsePrice` decimal(15,2) DEFAULT NULL,
  `Rate` double DEFAULT NULL,
  `Count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `materialcategory`
--

CREATE TABLE `materialcategory` (
  `ID` int(11) NOT NULL,
  `Value` varchar(50) DEFAULT NULL,
  `Tag` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `materialusedata`
--

CREATE TABLE `materialusedata` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `MaterialID` int(11) DEFAULT NULL,
  `EmployeeID` int(11) DEFAULT NULL,
  `SafeID` int(11) DEFAULT NULL,
  `SafeMovementID` int(11) DEFAULT NULL,
  `UseDate` datetime(3) DEFAULT NULL,
  `UseInfo` varchar(100) DEFAULT NULL,
  `UsePrice` decimal(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `payment`
--

CREATE TABLE `payment` (
  `ID` int(11) NOT NULL,
  `MethodID` int(11) DEFAULT NULL,
  `SafeID` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `EmployeeID` int(11) DEFAULT NULL,
  `Discount` decimal(15,2) DEFAULT NULL,
  `TotalPrice` decimal(15,2) DEFAULT NULL,
  `CashValue` decimal(15,2) DEFAULT NULL,
  `CardValue` decimal(15,2) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `paymentmethod`
--

CREATE TABLE `paymentmethod` (
  `ID` int(11) NOT NULL,
  `Value` varchar(50) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `safe`
--

CREATE TABLE `safe` (
  `ID` int(11) NOT NULL,
  `TotalPrice` decimal(15,2) DEFAULT NULL,
  `TotalMaterialPrice` decimal(15,2) DEFAULT NULL,
  `TotalMovementPrice` decimal(15,2) DEFAULT NULL,
  `MovementCount` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `PaymentStatus` tinyint(4) DEFAULT NULL,
  `IsOpen` tinyint(4) DEFAULT NULL,
  `CreatedDate` datetime(3) DEFAULT current_timestamp(3),
  `CreatedEmployeeID` int(11) DEFAULT NULL,
  `Tag` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `safemovement`
--

CREATE TABLE `safemovement` (
  `ID` int(11) NOT NULL,
  `SafeID` int(11) DEFAULT NULL,
  `EmployeeID` int(11) DEFAULT NULL,
  `TransactionID` int(11) DEFAULT NULL,
  `TransactionText` varchar(50) DEFAULT NULL,
  `CreatedDate` datetime(3) DEFAULT NULL,
  `Price` decimal(15,2) DEFAULT NULL,
  `IsFinish` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Görünüm yapısı durumu `vw_appointmentlist`
-- (Asıl görünüm için aşağıya bakın)
--
CREATE TABLE `vw_appointmentlist` (
`ID` int(11)
,`AppointmentDate` date
,`AppointmentTime` varchar(5)
,`FullDate` varchar(85)
,`StateOfDescription` varchar(28)
,`Fullname` varchar(32)
,`Phone` varchar(20)
,`JobField` varchar(120)
,`EmployeeID` int(11)
,`EmployeeName` varchar(50)
,`State` tinyint(3)
);

-- --------------------------------------------------------

--
-- Görünüm yapısı durumu `vw_businesslist`
-- (Asıl görünüm için aşağıya bakın)
--
CREATE TABLE `vw_businesslist` (
`ID` int(11)
,`TopCategoryID` int(11)
,`SubCategoryID` int(11)
,`TopCategoryValue` varchar(50)
,`SubCategoryValue` varchar(50)
,`Pricing` decimal(15,2)
,`Description` varchar(120)
,`NonCategory` tinyint(4)
,`BonusRatio` tinyint(4)
);

-- --------------------------------------------------------

--
-- Görünüm yapısı durumu `vw_listcustomer`
-- (Asıl görünüm için aşağıya bakın)
--
CREATE TABLE `vw_listcustomer` (
`ID` int(11)
,`Fullname` varchar(32)
,`Phone` varchar(20)
,`Tag` varchar(20)
);

-- --------------------------------------------------------

--
-- Görünüm yapısı durumu `vw_stateofdetail`
-- (Asıl görünüm için aşağıya bakın)
--
CREATE TABLE `vw_stateofdetail` (
`ID` int(11)
,`AppointmentID` int(11)
,`Fulldate` datetime
,`DetailText` text
,`StateValue` tinyint(4)
,`EmployeeID` int(11)
,`Fullname` varchar(50)
);

-- --------------------------------------------------------

--
-- Görünüm yapısı durumu `vw_sub_business_list`
-- (Asıl görünüm için aşağıya bakın)
--
CREATE TABLE `vw_sub_business_list` (
`ID` int(11)
,`Value` varchar(50)
,`Description` varchar(100)
);

-- --------------------------------------------------------

--
-- Görünüm yapısı durumu `vw_top_business_list`
-- (Asıl görünüm için aşağıya bakın)
--
CREATE TABLE `vw_top_business_list` (
`ID` int(11)
,`Value` varchar(50)
,`Description` varchar(100)
);

-- --------------------------------------------------------

--
-- Görünüm yapısı `vw_appointmentlist`
--
DROP TABLE IF EXISTS `vw_appointmentlist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_appointmentlist`  AS  select `ap`.`ID` AS `ID`,`ap`.`AppointmentDate` AS `AppointmentDate`,`ap`.`AppointmentTime` AS `AppointmentTime`,concat('<i class="fas fa-calendar-times"></i> ',`ap`.`AppointmentDate`,' - <i class="fas fa-clock"></i> ',`ap`.`AppointmentTime`) AS `FullDate`,case when `ap`.`State` = 0 then 'Müşteri Randevuya Bekleniyor' when `ap`.`State` = 1 then 'Müşteri Randevuya Geldi' when `ap`.`State` = 2 then 'Müşteri Randevuya Gelmedi' when `ap`.`State` = 3 then 'Randevu İptal Edildi' end AS `StateOfDescription`,`cus`.`Fullname` AS `Fullname`,`cus`.`Phone` AS `Phone`,`bus`.`Description` AS `JobField`,`ap`.`EmployeeID` AS `EmployeeID`,`emp`.`Fullname` AS `EmployeeName`,`ap`.`State` AS `State` from (((`appointment` `ap` join `customer` `cus` on(`cus`.`ID` = `ap`.`CustomerID`)) join `employee` `emp` on(`emp`.`ID` = `ap`.`EmployeeID`)) join `businesstransaction` `bus` on(`bus`.`ID` = `ap`.`BusinessID`)) ;

-- --------------------------------------------------------

--
-- Görünüm yapısı `vw_businesslist`
--
DROP TABLE IF EXISTS `vw_businesslist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_businesslist`  AS  select `bt`.`ID` AS `ID`,`bt`.`TopCategoryID` AS `TopCategoryID`,`bt`.`SubCategoryID` AS `SubCategoryID`,`btc`.`Value` AS `TopCategoryValue`,`bsc`.`Value` AS `SubCategoryValue`,`bt`.`Pricing` AS `Pricing`,`bt`.`Description` AS `Description`,`bt`.`NonCategory` AS `NonCategory`,`bt`.`BonusRatio` AS `BonusRatio` from ((`businesstransaction` `bt` join `businesstopcategory` `btc` on(`btc`.`ID` = `bt`.`TopCategoryID`)) join `businesssubcategory` `bsc` on(`bsc`.`ID` = `bt`.`SubCategoryID`)) ;

-- --------------------------------------------------------

--
-- Görünüm yapısı `vw_listcustomer`
--
DROP TABLE IF EXISTS `vw_listcustomer`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_listcustomer`  AS  select `customer`.`ID` AS `ID`,`customer`.`Fullname` AS `Fullname`,`customer`.`Phone` AS `Phone`,`customer`.`Tag` AS `Tag` from `customer` ;

-- --------------------------------------------------------

--
-- Görünüm yapısı `vw_stateofdetail`
--
DROP TABLE IF EXISTS `vw_stateofdetail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_stateofdetail`  AS  select `ad`.`ID` AS `ID`,`ad`.`AppointmentID` AS `AppointmentID`,`ad`.`Fulldate` AS `Fulldate`,`ad`.`DetailText` AS `DetailText`,`ad`.`StateValue` AS `StateValue`,`ad`.`EmployeeID` AS `EmployeeID`,`emp`.`Fullname` AS `Fullname` from (`appointment_detail` `ad` join `employee` `emp` on(`emp`.`ID` = `ad`.`EmployeeID`)) ;

-- --------------------------------------------------------

--
-- Görünüm yapısı `vw_sub_business_list`
--
DROP TABLE IF EXISTS `vw_sub_business_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_sub_business_list`  AS  select `businesssubcategory`.`ID` AS `ID`,`businesssubcategory`.`Value` AS `Value`,`businesssubcategory`.`Description` AS `Description` from `businesssubcategory` ;

-- --------------------------------------------------------

--
-- Görünüm yapısı `vw_top_business_list`
--
DROP TABLE IF EXISTS `vw_top_business_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_top_business_list`  AS  select `businesstopcategory`.`ID` AS `ID`,`businesstopcategory`.`Value` AS `Value`,`businesstopcategory`.`Description` AS `Description` from `businesstopcategory` ;

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `appointment_detail`
--
ALTER TABLE `appointment_detail`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `businesssubcategory`
--
ALTER TABLE `businesssubcategory`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `businesstopcategory`
--
ALTER TABLE `businesstopcategory`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `businesstransaction`
--
ALTER TABLE `businesstransaction`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `customersafedata`
--
ALTER TABLE `customersafedata`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `customersafemovementinfo`
--
ALTER TABLE `customersafemovementinfo`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `employeebonus`
--
ALTER TABLE `employeebonus`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `employeelog`
--
ALTER TABLE `employeelog`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `groupcustomer`
--
ALTER TABLE `groupcustomer`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `groupsafemovement`
--
ALTER TABLE `groupsafemovement`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `materialcategory`
--
ALTER TABLE `materialcategory`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `materialusedata`
--
ALTER TABLE `materialusedata`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `paymentmethod`
--
ALTER TABLE `paymentmethod`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `safe`
--
ALTER TABLE `safe`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `safemovement`
--
ALTER TABLE `safemovement`
  ADD PRIMARY KEY (`ID`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `appointment`
--
ALTER TABLE `appointment`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- Tablo için AUTO_INCREMENT değeri `appointment_detail`
--
ALTER TABLE `appointment_detail`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Tablo için AUTO_INCREMENT değeri `businesssubcategory`
--
ALTER TABLE `businesssubcategory`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Tablo için AUTO_INCREMENT değeri `businesstopcategory`
--
ALTER TABLE `businesstopcategory`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `businesstransaction`
--
ALTER TABLE `businesstransaction`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Tablo için AUTO_INCREMENT değeri `customer`
--
ALTER TABLE `customer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Tablo için AUTO_INCREMENT değeri `customersafemovementinfo`
--
ALTER TABLE `customersafemovementinfo`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `employee`
--
ALTER TABLE `employee`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `employeebonus`
--
ALTER TABLE `employeebonus`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
