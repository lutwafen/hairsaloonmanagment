-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 28 Kas 2019, 08:05:26
-- Sunucu sürümü: 10.4.8-MariaDB
-- PHP Sürümü: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `kuafor_app_demo`
--
CREATE DATABASE IF NOT EXISTS `kuafor_app_demo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `kuafor_app_demo`;

DELIMITER $$
--
-- Yordamlar
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcChangeStateOfAppointment` (IN `app_id` INT, IN `detail_text` TEXT, IN `state_value` TINYINT)  BEGIN
    INSERT INTO appointment_detail(AppointmentID, DetailText, StateValue) VALUES (app_id, detail_text, state_value);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcCreateAppointment` (IN `appointment_date` VARCHAR(50), IN `appointment_time` VARCHAR(50), IN `customer_id` INT, IN `employee_id` INT, IN `business_id` INT)  BEGIN
	INSERT INTO appointment (AppointmentDate, AppointmentTime, CustomerID, EmployeeID, BusinessID) 
	VALUES (appointment_date, appointment_time, customer_id, employee_id, business_id);
	
	SELECT LAST_INSERT_ID() AS AppointmentID;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `appointment`
--
-- Oluşturma: 27 Kas 2019, 10:05:42
--

CREATE TABLE IF NOT EXISTS `appointment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AppointmentDate` date DEFAULT NULL,
  `AppointmentTime` varchar(5) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `EmployeeID` int(11) DEFAULT NULL,
  `BusinessID` int(11) DEFAULT NULL,
  `State` tinyint(3) DEFAULT 0,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `appointment`
--

INSERT INTO `appointment` (`ID`, `AppointmentDate`, `AppointmentTime`, `CustomerID`, `EmployeeID`, `BusinessID`, `State`) VALUES
(1, '2019-11-16', '07:30', 1, 1, 1, 0),
(2, '2019-11-27', '11:00', 2, 1, 1, 0),
(3, '2019-11-27', '11:00', 2, 1, 1, 0),
(4, '2019-11-27', '11:00', 2, 1, 1, 0),
(5, '2019-11-27', '11:00', 2, 1, 1, 0),
(6, '2019-11-27', '11:00', 2, 1, 1, 0),
(7, '2019-11-27', '11:00', 2, 1, 1, 0),
(8, '2019-11-27', '11:00', 2, 1, 1, 0),
(9, '2019-11-27', '11:00', 2, 1, 1, 0),
(10, '2019-11-27', '11:00', 2, 1, 1, 0),
(11, '2019-11-27', '11:00', 2, 1, 1, 0),
(12, '2019-11-27', '11:00', 2, 1, 1, 0),
(13, '2019-11-27', '11:00', 2, 1, 1, 0),
(14, '2019-11-27', '11:00', 2, 1, 1, 0),
(15, '2019-11-27', '11:00', 2, 1, 1, 0),
(23, '2019-11-27', '11:43', 1, 2, 1, 0),
(24, '2019-11-27', '11:43', 1, 2, 1, 0),
(25, '2019-11-27', '11:43', 1, 2, 1, 0),
(26, '2019-11-27', '11:43', 1, 2, 1, 0),
(27, '2019-11-27', '11:43', 1, 2, 1, 0),
(28, '2019-11-27', '11:43', 1, 2, 1, 0),
(29, '2019-11-27', '11:43', 1, 2, 1, 0),
(30, '2019-11-27', '11:43', 1, 2, 1, 0),
(31, '2019-11-27', '11:43', 1, 2, 1, 0),
(32, '2019-11-27', '11:43', 1, 2, 1, 0),
(33, NULL, NULL, NULL, NULL, NULL, 0),
(34, NULL, NULL, NULL, NULL, NULL, 0),
(35, '2019-11-27', '11:48', 3, 1, 1, 0),
(36, '2019-11-27', '11:48', 3, 1, 1, 0),
(37, '2019-11-27', '11:48', 3, 1, 1, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `appointment_detail`
--
-- Oluşturma: 28 Kas 2019, 06:58:19
-- Son güncelleme: 28 Kas 2019, 06:58:23
--

CREATE TABLE IF NOT EXISTS `appointment_detail` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AppointmentID` int(11) NOT NULL,
  `Fulldate` datetime NOT NULL DEFAULT current_timestamp(),
  `DetailText` text NOT NULL,
  `StateValue` tinyint(4) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `businesssubcategory`
--
-- Oluşturma: 22 Kas 2019, 11:03:53
--

CREATE TABLE IF NOT EXISTS `businesssubcategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Value` varchar(50) NOT NULL,
  `Description` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `businesssubcategory`
--

INSERT INTO `businesssubcategory` (`ID`, `Value`, `Description`) VALUES
(1, 'Bacak', 'Ağda Kategorisinin Bir Alt Kategorisi');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `businesstopcategory`
--
-- Oluşturma: 22 Kas 2019, 11:03:26
--

CREATE TABLE IF NOT EXISTS `businesstopcategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Value` varchar(50) NOT NULL,
  `Description` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `businesstopcategory`
--

INSERT INTO `businesstopcategory` (`ID`, `Value`, `Description`) VALUES
(1, 'Ağda', 'Genel Bir Kategoridir');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `businesstransaction`
--
-- Oluşturma: 16 Kas 2019, 02:06:39
--

CREATE TABLE IF NOT EXISTS `businesstransaction` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TopCategoryID` int(11) DEFAULT NULL,
  `SubCategoryID` int(11) DEFAULT NULL,
  `Pricing` decimal(15,2) DEFAULT NULL,
  `Description` varchar(120) DEFAULT NULL,
  `NonCategory` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `businesstransaction`
--

INSERT INTO `businesstransaction` (`ID`, `TopCategoryID`, `SubCategoryID`, `Pricing`, `Description`, `NonCategory`) VALUES
(1, 1, 1, '32.00', 'Ağda <i class=\"fas fa-random\"></i> Bacak', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `customer`
--
-- Oluşturma: 16 Kas 2019, 02:09:02
--

CREATE TABLE IF NOT EXISTS `customer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Fullname` varchar(32) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `Tag` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `customer`
--

INSERT INTO `customer` (`ID`, `Fullname`, `Phone`, `Tag`) VALUES
(1, 'Burcu Babacan', ' 0 535 684 98 39', 'burcu-babacan'),
(2, 'İrem Kunt', '0 (535) 442 20 33', 'irem-kunt'),
(3, 'Ümran Türkdoğan', '+90 (508) 535 48 27', 'umran-turkdogan');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `customersafedata`
--
-- Oluşturma: 15 Kas 2019, 02:22:28
--

CREATE TABLE IF NOT EXISTS `customersafedata` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `TotalPrice` decimal(15,2) DEFAULT NULL,
  `SafeID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `customersafemovementinfo`
--
-- Oluşturma: 15 Kas 2019, 02:22:28
--

CREATE TABLE IF NOT EXISTS `customersafemovementinfo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) DEFAULT NULL,
  `TransactionID` int(11) DEFAULT NULL,
  `TransactionPrice` decimal(15,2) DEFAULT NULL,
  `EmployeeID` int(11) DEFAULT NULL,
  `SafeID` int(11) DEFAULT NULL,
  `SafeMovementID` int(11) DEFAULT NULL,
  `CreatedAt` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `employee`
--
-- Oluşturma: 16 Kas 2019, 04:45:42
--

CREATE TABLE IF NOT EXISTS `employee` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Fullname` varchar(50) DEFAULT NULL,
  `Username` varchar(16) DEFAULT NULL,
  `Password` varchar(16) DEFAULT NULL,
  `Address` varchar(100) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT NULL,
  `IsAdmin` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `employee`
--

INSERT INTO `employee` (`ID`, `Fullname`, `Username`, `Password`, `Address`, `Phone`, `IsActive`, `IsAdmin`) VALUES
(1, 'Fatih Çalışan', 'fatih', '1234', 'Lorem Ipsum Sit Dolor', '0000', 1, 1),
(2, 'Burcu Beşok', ' burcsok', '1234', 'Canberk Sokak 748 08215 Uşak', '+90 530 890 81 8', 1, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `employeelog`
--
-- Oluşturma: 15 Kas 2019, 02:22:28
--

CREATE TABLE IF NOT EXISTS `employeelog` (
  `ID` int(11) NOT NULL,
  `EmployeeID` int(11) DEFAULT NULL,
  `LogReg` text DEFAULT NULL,
  `LogDate` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `groupcustomer`
--
-- Oluşturma: 15 Kas 2019, 02:22:29
--

CREATE TABLE IF NOT EXISTS `groupcustomer` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `GroupID` int(11) DEFAULT NULL,
  `MovementCount` int(11) DEFAULT NULL,
  `PricingStatus` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `groups`
--
-- Oluşturma: 15 Kas 2019, 02:22:28
--

CREATE TABLE IF NOT EXISTS `groups` (
  `ID` int(11) NOT NULL,
  `CreatedCustomerID` int(11) DEFAULT NULL,
  `SafeID` int(11) DEFAULT NULL,
  `NumberOfPeople` int(11) DEFAULT NULL,
  `Date` datetime(3) DEFAULT NULL,
  `Tag` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `groupsafemovement`
--
-- Oluşturma: 15 Kas 2019, 02:22:29
--

CREATE TABLE IF NOT EXISTS `groupsafemovement` (
  `ID` int(11) NOT NULL,
  `SafeMovementID` int(11) DEFAULT NULL,
  `SafeID` int(11) DEFAULT NULL,
  `GCustomerID` int(11) DEFAULT NULL,
  `GroupID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `material`
--
-- Oluşturma: 15 Kas 2019, 02:22:29
--

CREATE TABLE IF NOT EXISTS `material` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `CategoryID` int(11) DEFAULT NULL,
  `Price` decimal(15,2) DEFAULT NULL,
  `UsePrice` decimal(15,2) DEFAULT NULL,
  `Rate` double DEFAULT NULL,
  `Count` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `materialcategory`
--
-- Oluşturma: 15 Kas 2019, 02:22:29
--

CREATE TABLE IF NOT EXISTS `materialcategory` (
  `ID` int(11) NOT NULL,
  `Value` varchar(50) DEFAULT NULL,
  `Tag` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `materialusedata`
--
-- Oluşturma: 15 Kas 2019, 02:22:29
--

CREATE TABLE IF NOT EXISTS `materialusedata` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `MaterialID` int(11) DEFAULT NULL,
  `EmployeeID` int(11) DEFAULT NULL,
  `SafeID` int(11) DEFAULT NULL,
  `SafeMovementID` int(11) DEFAULT NULL,
  `UseDate` datetime(3) DEFAULT NULL,
  `UseInfo` varchar(100) DEFAULT NULL,
  `UsePrice` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `payment`
--
-- Oluşturma: 15 Kas 2019, 02:22:30
--

CREATE TABLE IF NOT EXISTS `payment` (
  `ID` int(11) NOT NULL,
  `MethodID` int(11) DEFAULT NULL,
  `SafeID` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `EmployeeID` int(11) DEFAULT NULL,
  `Discount` decimal(15,2) DEFAULT NULL,
  `TotalPrice` decimal(15,2) DEFAULT NULL,
  `CashValue` decimal(15,2) DEFAULT NULL,
  `CardValue` decimal(15,2) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `paymentmethod`
--
-- Oluşturma: 15 Kas 2019, 02:22:30
--

CREATE TABLE IF NOT EXISTS `paymentmethod` (
  `ID` int(11) NOT NULL,
  `Value` varchar(50) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `safe`
--
-- Oluşturma: 15 Kas 2019, 02:22:30
--

CREATE TABLE IF NOT EXISTS `safe` (
  `ID` int(11) NOT NULL,
  `TotalPrice` decimal(15,2) DEFAULT NULL,
  `TotalMaterialPrice` decimal(15,2) DEFAULT NULL,
  `TotalMovementPrice` decimal(15,2) DEFAULT NULL,
  `MovementCount` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `PaymentStatus` tinyint(4) DEFAULT NULL,
  `IsOpen` tinyint(4) DEFAULT NULL,
  `CreatedDate` datetime(3) DEFAULT NULL,
  `CreatedEmployeeID` int(11) DEFAULT NULL,
  `Tag` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `safemovement`
--
-- Oluşturma: 15 Kas 2019, 02:22:30
--

CREATE TABLE IF NOT EXISTS `safemovement` (
  `ID` int(11) NOT NULL,
  `SafeID` int(11) DEFAULT NULL,
  `EmployeeID` int(11) DEFAULT NULL,
  `TransactionID` int(11) DEFAULT NULL,
  `TransactionText` varchar(50) DEFAULT NULL,
  `CreatedDate` datetime(3) DEFAULT NULL,
  `Price` decimal(15,2) DEFAULT NULL,
  `IsFinish` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Görünüm yapısı durumu `vw_appointmentlist`
-- (Asıl görünüm için aşağıya bakın)
--
CREATE TABLE IF NOT EXISTS `vw_appointmentlist` (
`ID` int(11)
,`AppointmentDate` date
,`AppointmentTime` varchar(5)
,`FullDate` varchar(85)
,`StateOfDescription` varchar(28)
,`Fullname` varchar(32)
,`Phone` varchar(20)
,`JobField` varchar(120)
,`EmployeeID` int(11)
,`EmployeeName` varchar(50)
,`State` tinyint(3)
);

-- --------------------------------------------------------

--
-- Görünüm yapısı durumu `vw_businesslist`
-- (Asıl görünüm için aşağıya bakın)
--
CREATE TABLE IF NOT EXISTS `vw_businesslist` (
`ID` int(11)
,`TopCategoryID` int(11)
,`SubCategoryID` int(11)
,`TopCategoryValue` varchar(50)
,`SubCategoryValue` varchar(50)
,`Pricing` decimal(15,2)
,`Description` varchar(120)
,`NonCategory` tinyint(4)
);

-- --------------------------------------------------------

--
-- Görünüm yapısı durumu `vw_listcustomer`
-- (Asıl görünüm için aşağıya bakın)
--
CREATE TABLE IF NOT EXISTS `vw_listcustomer` (
`ID` int(11)
,`Fullname` varchar(32)
,`Phone` varchar(20)
,`Tag` varchar(20)
);

-- --------------------------------------------------------

--
-- Görünüm yapısı durumu `vw_stateofdetail`
-- (Asıl görünüm için aşağıya bakın)
--
CREATE TABLE IF NOT EXISTS `vw_stateofdetail` (
`ID` int(11)
,`AppointmentID` int(11)
,`Fulldate` datetime
,`DetailText` text
);

-- --------------------------------------------------------

--
-- Görünüm yapısı `vw_appointmentlist`
--
DROP TABLE IF EXISTS `vw_appointmentlist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_appointmentlist`  AS  select `ap`.`ID` AS `ID`,`ap`.`AppointmentDate` AS `AppointmentDate`,`ap`.`AppointmentTime` AS `AppointmentTime`,concat('<i class="fas fa-calendar-times"></i> ',`ap`.`AppointmentDate`,' - <i class="fas fa-clock"></i> ',`ap`.`AppointmentTime`) AS `FullDate`,case when `ap`.`State` = 0 then 'Müşteri Randevuya Bekleniyor' when `ap`.`State` = 1 then 'Müşteri Randevuya Geldi' when `ap`.`State` = 2 then 'Müşteri Randevuya Gelmedi' when `ap`.`State` = 3 then 'Randevu İptal Edildi' end AS `StateOfDescription`,`cus`.`Fullname` AS `Fullname`,`cus`.`Phone` AS `Phone`,`bus`.`Description` AS `JobField`,`ap`.`EmployeeID` AS `EmployeeID`,`emp`.`Fullname` AS `EmployeeName`,`ap`.`State` AS `State` from (((`appointment` `ap` join `customer` `cus` on(`cus`.`ID` = `ap`.`CustomerID`)) join `employee` `emp` on(`emp`.`ID` = `ap`.`EmployeeID`)) join `businesstransaction` `bus` on(`bus`.`ID` = `ap`.`BusinessID`)) ;

-- --------------------------------------------------------

--
-- Görünüm yapısı `vw_businesslist`
--
DROP TABLE IF EXISTS `vw_businesslist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_businesslist`  AS  select `bt`.`ID` AS `ID`,`bt`.`TopCategoryID` AS `TopCategoryID`,`bt`.`SubCategoryID` AS `SubCategoryID`,`btc`.`Value` AS `TopCategoryValue`,`bsc`.`Value` AS `SubCategoryValue`,`bt`.`Pricing` AS `Pricing`,`bt`.`Description` AS `Description`,`bt`.`NonCategory` AS `NonCategory` from ((`businesstransaction` `bt` join `businesstopcategory` `btc` on(`btc`.`ID` = `bt`.`TopCategoryID`)) join `businesssubcategory` `bsc` on(`bsc`.`ID` = `bt`.`SubCategoryID`)) ;

-- --------------------------------------------------------

--
-- Görünüm yapısı `vw_listcustomer`
--
DROP TABLE IF EXISTS `vw_listcustomer`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_listcustomer`  AS  select `customer`.`ID` AS `ID`,`customer`.`Fullname` AS `Fullname`,`customer`.`Phone` AS `Phone`,`customer`.`Tag` AS `Tag` from `customer` ;

-- --------------------------------------------------------

--
-- Görünüm yapısı `vw_stateofdetail`
--
DROP TABLE IF EXISTS `vw_stateofdetail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_stateofdetail`  AS  select `appointment_detail`.`ID` AS `ID`,`appointment_detail`.`AppointmentID` AS `AppointmentID`,`appointment_detail`.`Fulldate` AS `Fulldate`,`appointment_detail`.`DetailText` AS `DetailText` from `appointment_detail` ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
