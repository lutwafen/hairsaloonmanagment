<?php
defined('BASEPATH') or exit('No direct script access allowed');
$route['default_controller'] = 'Panel';
$route['404_override'] = '';
$route['translate_uri_dashes'] = false;
$route['RandevuGoster/(:any)'] = 'Appointment/ShowAppointment/$1';
$route['RandevuIslemGuncelle/(:any)'] = 'Appointment/changeAppointmentState/$1';
$route['Randevu/Olustur'] = 'Appointment/CreateAppointment';
$route['Randevu/Tekrarla'] = 'Appointment/RepeatAppointment';
$route['Is'] = 'Business';
$route['Kategori/Alt/Olustur/(:num)'] = 'Business/AddSubCategory/$1';
