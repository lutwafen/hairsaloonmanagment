<?php
class Appointment extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        ControlOfLogin();
    }

    // Randevu gösterme
    public function ShowAppointment($app_id)
    {
        loadModel('Appointment');
        $viewData = new stdClass();
        $viewData->AppointmentID = 0;
        $appointmentData = $this->Appointment->getAppointment($app_id);
        if (count($appointmentData) > 0) {
            $viewData->AppointmentID = $app_id;
            $viewData->appointmentData = $appointmentData[0];
            if ($viewData->appointmentData->State != 0) {
                $viewData->appState = $this->Appointment->StateOfDetail($app_id);
            }
        }
        $this->load->view('Appointment/AppContent_View', $viewData);
    }

    // Randevu Tekrarlama
    public function RepeatAppointment()
    {
        $result_data = array();
        if (post('recurring_app_id')) {
            $id = post('recurring_app_id');
            $time = post('appointment_time');
            $date = post('appointment_date');
            $appointment_data = array('recurring_id' => $id, 'appointment_time' => $time, 'appointment_date' => $date);

            loadModel('Appointment');

            $execute = $this->Appointment->RepeatAppointment($appointment_data);
            if ($execute > 0) {
                $redirect_link = base_url() . 'RandevuGoster/' . $execute[0]->AppointmentID;
                $result_data['state'] = true;
                $result_data['redirect_link'] = $redirect_link;
                $result_data['message'] = "Randevu Başarıyla Tekrarlandı!";
            } else {
                $result_data['state'] = false;
                $result_data['redirect_link'] = "";
                $result_data['message'] = "Randevu Tekrarlanamadı!";
            }
        } else {
            $result_data['state'] = false;
            $result_data['redirect_link'] = "";
            $result_data['message'] = "Hatalı bir işlem yapıldı!";
        }

        echo json_encode($result_data, JSON_UNESCAPED_UNICODE);
    }

    // Randevu Durum Değiştirme
    public function changeAppointmentState($appointment_id)
    {
        if (post('app_id')) {
            $id = $appointment_id;
            $state_type = post('state_type') == 0 ? "0" : post('state_type');
            $state_text = post('state_text');
            loadModel('Appointment');
            $data = array("app_id" => $id, "detail_text" => $state_text, "state_value" => $state_type, "emp_id" => $this->session->EmployeeID);
            $result = $this->Appointment->SetAppointmentState($data);
            echo $result ? true : false;
        }

        return false;
    }
    // Randevu Oluştur
    public function CreateAppointment()
    {
        if (post('customer_id')) {
            $appointment_data = array(
                "appointment_date" => post("appointment_date"),
                "appointment_time" => post("appointment_time"),
                "customer_id" => post("customer_id"),
                "employee_id" => post("employee_id"),
                "business_id" => post("business_id"));
            //print_r($appointment_data);
            loadModel("Appointment");
            $viewData = new stdClass();
            $result = $this->Appointment->CreateAppointment($appointment_data);
            if ($result) {
                $viewData->AppointmentID = $result[0]->AppointmentID;
                if ($viewData->AppointmentID > 0) {
                    reConnect();
                    $viewData->appointmentData = $this->Appointment->getAppointment($viewData->AppointmentID)[0];
                    //print_r($viewData->appointmentData);
                }
                redirect(base_url() . 'RandevuGoster/' . $viewData->AppointmentID);
            } else {
                $viewData->error = "Bir hata var lütfen daha sonra tekrar deneyin";
                echo $viewData->error;
            }
        } else {

            loadModel('Employee');
            loadModel('Business');
            $viewData = new stdClass();
            $viewData->EmployeeList = $this->Employee->Employees(); // Employee Data
            // Business Select fields
            $select = "TopCategoryID, TopCategoryValue";
            $viewData->TopBusinessCategory = $this->Business->listOfBusiness($select); // TopBusinessCategory
            $this->load->view('Appointment/CreateAppointment_view', $viewData);
        }
    }
}
