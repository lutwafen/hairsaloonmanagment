<?php
class Business extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        ControlOfLogin();
        loadModel('Business');
    }

    public function index()
    {
        $viewData = new stdClass();
        //$viewData->BusinessList = $this->Business->listOfBusiness("*");
        $config['select_fields'] = 'ID, Value';
        $config['which_view'] = 'vw_top_business_list';
        $viewData->BusinessList = $this->Business->getBusinessList($config);
        $this->load->view('Business/Home_view', $viewData);
    }


    public function ShowBusinessField($business_id){
      echo $business_id;
    }

    public function getSubCategory()
    {
        if (isPost()) {
            $select = "*";

            $where = array("TopCategoryID" => post('TopCategoryID'));
            $data = $this->Business->listOfBusiness($select, $where);
            if (!empty($data)) {
                echo json_encode($data, JSON_UNESCAPED_UNICODE);
            } else {
                echo 0;
            }
        }
    }

    public function AddSubCategory($top_category_id)
    {
        $viewData = new stdClass();
        $viewData->Business = $this->Business->listOfBusiness("TopCategoryID, TopCategoryValue", array('TopCategoryID' => $top_category_id))[0];
        $this->load->view('Business/CreateSubCategory_view', $viewData);
    }

    public function CreateSubCategory()
    {
        if(post('create_sub_category'))
        {
            $sub_category_value = post('create_sub_category');
            $sub_category_id = $this->Business->CreateSubCategory(array($sub_category_value));
            $returned_data["sub_category_id"] = $sub_category_id[0]->LastSubCategoryID;
            $returned_data['sub_category_value'] = $sub_category_value;
            echo json_encode($returned_data, JSON_UNESCAPED_UNICODE);
        }
    }

    public function CreateBusinessField()
    {
        if(isPost())
        {
            $data = array(
                          "top_category_id" => post('top_category_id'),
                          "sub_category_id" => post('sub_category_id'),
                          "price_value" => post('price_value'),
                          "bonus_ratio" => post('bonus_ratio')
                          );
            $result = $this->Business->CreateBusinessField($data);
            if($result)
            {
                echo 1;
            }
            else{
                echo 0;
            }
        }
    }

    public function SearchSubCategory()
    {
        if (post('subCategoryValue')) {
            $search_result = $this->Business->listOfSubBusiness("ID, Value", array(), array('Value' => post('subCategoryValue')));
            $output = '';
            if (count($search_result) > 0) {
                foreach ($search_result as $result) {
                    $output .= '<button
                                type="button"
                                class="btnSelectSubCategory list-group-item"
                                data-sub-category-id="' . $result->ID . '"
                                data-sub-category-value="' . $result->Value . '">';
                    $output .= $result->Value . ' <i class="fas fa-check-double"></i>';
                    $output .= '</button>';
                }
                $output .= '<button type="button" class="btnCreateSubCategory list-group-item bg-warning">';
                $output .= 'Bu İşlemi Oluştur <i class="fas fa-plus"></i>';
                $output .= '</button>';
            } else {
                $output .= '<button type="button" class="btnCreateSubCategory list-group-item bg-warning">';
                $output .= 'Bu İşlemi Oluştur <i class="fas fa-plus"></i>';
                $output .= '</button>';
            }
            echo $output;
        }
    }

    public function EditBusinessField()
    {
      if(post('business_id'))
      {
        $data = array(
          "business_id" => post('business_id'),
          "pricing" => post('pricing'),
          "bonus_ratio" => post('bonus_ratio')
        );

        if(ExecuteProcedure("PrcUpdateBusinessField", $data))
        {
          echo "ok";
        }
        else {
          echo "cancel";
        }

      }
    }

    public function ChangeBusinessActive(){
      if(post('business_id'))
      {
        $data = array('business_id' => post('business_id'), 'change_value' => post('change_value'));
        if(ExecuteProcedure("PrcBusinessActiveChange", $data))
        {
          echo true;
        }
        else{
          echo false;
        }
      }
    }

}
