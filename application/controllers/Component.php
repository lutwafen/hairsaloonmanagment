<?php
class Component extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function RouteComponent()
    {
        if (isPost()) {
            $component_id = post("component_id");
            $component_url = 'Components/' . post('component_url');
            switch ($component_id) {
                case "randevu_1": // Randevu Oluşturma
                    loadModel('Employee');
                    loadModel('Business');
                    // Randevu oluştur
                    $viewData = new stdClass();
                    $viewData->EmployeeList = $this->Employee->Employees(); // Employee Data
                    // Business Select fields
                    $select = "TopCategoryID, TopCategoryValue";
                    $viewData->TopBusinessCategory = $this->Business->listOfBusiness($select); // TopBusinessCategory
                    $this->load->view($component_url . 'CreateAppointment_Component', $viewData);
                    break;
                case "randevu_2": // Var Olan Randevuları Gösterme
                    loadModel('Appointment');

                    $viewData = new stdClass();
                    $app_where = array();

                    $app_where['AppointmentDate'] = date('Y-m-d');
                    $app_where['State'] = 0;
                    $app_where['EmployeeID'] = $this->session->EmployeeID;

                    $viewData->AppointmentList = $this->Appointment->AppointmentList($app_where, "alist");
                    if (count($viewData->AppointmentList) > 0) {
                        $this->load->view($component_url . 'DailyOperations_Component', $viewData);
                    } else {
                        echo '
                            <div class="col-12">
                                <div class="alert alert-danger p-2 my-2">
                                    Bekleyen veya Yaklaşan Bir Randevu Bulunamadı!
                                </div>
                            </div>
                        ';
                    }
                    break;
                case 'listOfBusiness': // Seçilen Üst kategoriye göre alt kategorilerin listesi
                    loadModel('Business');
                    $top_category_id = post('category_id');
                    $select_fields = "*";
                    $where_params = array(
                        "TopCategoryID" => $top_category_id
                    );

                    $viewData = new stdClass();
                    $business_list = $this->Business->listOfBusiness($select_fields, $where_params);
                    $viewData->BusinessList = $business_list;
                    $viewData->TopCategoryID = $top_category_id;
                    $this->load->view($component_url . 'ListSelectCategoryBusiness_Component', $viewData);
                    break;
                case 'editBusinessField': // Seçilen Kategoriye Göre Ona Ait İş Biriminin Düzenlenmesi
                  loadModel('Business');
                  $edit_business_field_id = post('business_field_id');

                  $config['select_fields'] = "*";
                  $config['which_view'] = 'vw_businesslist';
                  $config['limit'] = 5;
                  $where_params = array('ID' => $edit_business_field_id);
                  $viewData = new stdClass();
                  $viewData->businessData = $this->Business->getBusinessList($config,  $where_params);

                  if(count($viewData->businessData) > 0)
                  {
                    $viewData->businessData = $viewData->businessData[0];
                    $this->load->view('Components/Business/EditFieldModal_Component', $viewData);
                  }
                  else{
                    echo '
                      <script> alert("Bir Bilgi Bulunamadı!") </script>
                    ';
                  }
                break;
                default: // Eksik Veya Yanlış Bilgi
                    $output = '<div class="col-12">';
                    $output .= '<div class="alert alert-danger p-2">';
                    $output .= '<strong> Aradığınız Görünüm Bulunamadı!</strong>';
                    $output .= '</div>';
                    $output .= '</div>';
                    break;
            }
        } else {
            echo "post yok";
        }
    }
}
