<?php
    class Customer extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            loadModel('Customer');
        }

        function SearchByTag()
        {
            if(isPost())
            {
                $tag = convertToTAG(post('tag'));
                $list = post('list') ? post('list') : false;
                $result = $this->Customer->searchForTag($tag);
                if($result == false)
                {
                    echo $list ? "<strong> Aradığınız Müşteri Bulunamadı!</strong>" : 0;
                }
                else{
                    if($list)
                    {
                        $viewData = new stdClass();
                        $viewData->searchResult = $result;
                        $this->load->view('Components/Customer/CustomerSearchList_Component', $viewData);
                    }
                    else{
                        echo json_encode($result, JSON_UNESCAPED_UNICODE);
                    }
                }
            }
        }
    }