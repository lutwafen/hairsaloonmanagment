<?php 
    class Login extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            $this->load->model('Employee_Model', 'Employee');
        }

        function index()
        {
            $this->load->view('Include/Header');
            $this->load->view("login_v");
            $this->load->view('Include/Footer');
        }

        # [POST], Giriş Action
        function Login(){
            $data = array("login" => false);
            if(isPost())
            {
                $login_data = $this->Employee->Login(post("username"), post("password"));
                if(!empty($login_data))
                {
                    $data['login'] = true;
                    $this->session->Employee = $login_data;
                    $this->session->IsAdmin = $login_data->IsAdmin;
                    $this->session->EmployeeID = $login_data->ID;
                }
                else{
                    $data['error_msg'] = "Kullanıcı adı veya Parola hatalı olabilir";
                }
            }
            
            echo json_encode($data, JSON_UNESCAPED_UNICODE);
        }

        # [POST], Bir personelin olup olmadığını kontrol eden action
        function EmployeeControl()
        {
            $data = array("find" => false);
            if(isPost())
            {
                $findOfEmployee = $this->Employee->FindOfEmployee(post("username"));
                if($findOfEmployee > 0)
                    $data['find'] = true;
            }

            echo json_encode($data, JSON_UNESCAPED_UNICODE);
        }

        # [REQUEST] Çıkış Action
        function Logout()
        {
            $this->session->sess_destroy();
            redirect(base_url());
        }
    }