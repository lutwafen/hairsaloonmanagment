<?php
    class Navbar extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
        }

        function UpcommingAppointment()
        {
        
            loadModel("Appointment");
            $data = array();
            $appointment_data = isset($this->Appointment->EmpUpcomingAppointment($this->session->EmployeeID)[0]) ? $this->Appointment->EmpUpcomingAppointment($this->session->EmployeeID)[0] :"0";
            if($appointment_data != "0")
            {
                $viewData = new stdClass();
                $viewData->appointment_data = $appointment_data;
                echo  $this->load->view('Render/render_last_appointment_v', $viewData,true);
            }
            else{
                echo '<strong>Yaklaşan Bir Randevu Yok!</strong>';
            }
        }

        function UpcommingAppointmentControl()
        {
            echo json_encode(array("upcAppointment" => empUpcomingAppControl()), JSON_UNESCAPED_UNICODE);
        }



    }