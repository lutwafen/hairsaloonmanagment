<?php
class Panel extends CI_Controller
{
    private $viewData = null;
    public function __construct()
    {
        parent::__construct();
        ControlOfLogin();
        $this->viewData = new stdClass();
    }

    public function Index()
    {

        $this->load->view("panel_v");
    }

}
