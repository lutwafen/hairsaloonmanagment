<?php  
    function empUpcomingAppControl()
    {
        $ci = get_instance();
        $ci->load->model("Appointment_Model", "Appointment");
        return empty($ci->Appointment->EmpUpcomingAppointment($ci->session->EmployeeID)) ? false : true;
    }