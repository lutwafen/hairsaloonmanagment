<?php

// Giriş kontrolü
function ControlOfLogin()
{
    $ci = get_instance();
    if (!isset($ci->session->Employee)) {
        redirect(base_url("Login"));
    }
}

// Post Kontrolü
function isPost()
{
    if (isset($_POST)) {
        return true;
    }
    return false;
}

// Post ile gelen verinin alınması
function post($value)
{
    $ci = get_instance();
    if ($ci->input->post($value)) {
        return $ci->input->post($value);
    }

}

// Veritabanına Yeniden Bağlantı
function reConnect()
{
    $ci = get_instance();
    $ci->db->close();
    $ci->db->initialize();
}

// Model dosyalarının kolayca yüklenmesi için yazılan basit mini fonksiyon
function loadModel($model_name)
{
    $ci = get_instance();
    $ci->load->model($model_name . '_Model', $model_name);
}

// Metinlerin Etiketsel Dönüşümü
function convertToTAG($text)
{
    $turkce = array("ç", "Ç", "ğ", "Ğ", "ü", "Ü", "ö", "Ö", "ı", "İ", "ş", "Ş", ".", ",", "!", "'", "\"", " ", "?", "*", "_", "|", "=", "(", ")", "[", "]", "{", "}");
    $convert = array("c", "c", "g", "g", "u", "u", "o", "o", "i", "i", "s", "s", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-");
    return strtolower(str_replace($turkce, $convert, $text));
}

// Procedure Parametre Ayarlama
function callProcedure($prcName, $dataSize)
{
    $sql = "CALL $prcName(";
    for ($i = 0; $i < $dataSize; $i++) {
        $sql .= "?,";
    }
    $sql = rtrim($sql, ",");
    $sql .= ")";
    return $sql;
}

// Sisteme giriş yapan kullanıcının yönetici olup olmadığını kontrol eden fonksiyon
function IsAdmin(){
  $ci = get_instance();
  return $ci->session->IsAdmin;
}

// Procedure İşlemlerini hızlıca çalıştırabilmek amaçlı yazılan fonksiyon
function ExecuteProcedure($prcName, $prcData = array())
{
  reConnect();
  $ci = get_instance();
  if(count($prcData) > 0)
  {
    $procedure = callProcedure($prcName, count($prcData));
    $query = $ci->db->query($procedure, $prcData);
    if($query !== null)
      return true;
  }
  return false;
}

// İçerisinde Select İle Alınması Gereken Bir Veri Olduğu Zaman O Proeceduru Çalıştırma

function SelectProcedure($prcName, $prcData)
{
  reConnect();
  $ci = get_instance();
  if(count($prcData) > 0)
  {
    $procedure = callProcedure($prcName, count($prcData));
    $query = $ci->db->query($procedure, $prcData);
    if($query !== null)
      return $query->result();
  }
  return false;
}
