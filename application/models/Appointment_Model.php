<?php
class Appointment_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        reConnect();
    }

    public function AppointmentList($where = array(), $tableTag)
    {
        $this->db->select("*");
        $this->db->from("vw_appointmentlist {$tableTag}");
        $this->db->where($where);
        return $this->db->get()->result();
    }

    public function getAppointment($app_id)
    {
        $this->db->select('*');
        $this->db->from('vw_appointmentlist alist');
        $this->db->where(array('alist.ID' => $app_id));
        return $this->db->get()->result();
    }

    public function StateOfDetail($app_id)
    {
        return $this->db->query("SELECT * FROM vw_stateofdetail ad WHERE ad.AppointmentID = $app_id")->row();
    }

    public function SetAppointmentState($data = array())
    {
        if (!empty($data)) {
            $procedure = callProcedure("prcChangeStateOfAppointment", count($data));
            $result = $this->db->query($procedure, $data);
            if ($result !== null) {
                return true;
            }
        }
        return false;
    }

    public function RepeatAppointment($data = array())
    {
        if (!empty($data)) {
            $procedure = callProcedure("prcRepeatAppointment", count($data));
            $result = $this->db->query($procedure, $data);
            if ($result !== null) {
                return $result->result();
            }
        }
        return 0;
    }

    public function EmpUpcomingAppointment($EmployeeID)
    {
        $this->db->select("*");
        $this->db->from('vw_appointmentlist alist');
        $this->db->where(array("alist.EmployeeID" => $EmployeeID, "alist.State" => 0, "alist.AppointmentDate" => date("Y-m-d")));
        $this->db->order_by('alist.AppointmentTime', 'desc');
        $this->db->limit(1);
        return $this->db->get()->result();
    }

    public function CreateAppointment($params = array())
    {
        if (!empty($params)) {
            $procedure = callProcedure("prcCreateAppointment", count($params));
            $result = $this->db->query($procedure, $params);
            if ($result !== null) {
                return $result->result();
            }

        }
        return false;
    }
}
