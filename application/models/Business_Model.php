<?php
class Business_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }


    #region Geçerliliği olmayan fonksiyonlar
    public function listOfBusiness($select_fields, $where_params = array(), $like_params = array())
    {
        $this->db->select("$select_fields");
        $this->db->from("vw_businesslist");
        $this->db->where($where_params);
        if (!empty($like_params)) {
            foreach ($like_params as $field => $value) {
                $this->db->like($field, $value);
            }
        }
        $this->db->order_by('ID', "desc");
        return $this->db->get()->result();
    }


    function listOfSubBusiness($select_fields, $where_params = array(), $like_params = array())
    {
      $this->db->select("$select_fields");
      $this->db->from("vw_sub_business_list");
      $this->db->where($where_params);
      if (!empty($like_params)) {
          foreach ($like_params as $field => $value) {
              $this->db->like($field, $value);
          }
      }
      $this->db->order_by('ID', "desc");
      return $this->db->get()->result();
    }

    #endregion



    function getBusinessList($config = array(), $where_params = array(), $like_params = array()){

        ## Config
        $select_fields = isset($config['select_fields']) ? $config['select_fields'] : "*";
        $view_name = isset($config['which_view']) ? $config['which_view'] : "businesstransaction";

        $this->db->select("$select_fields");
        $this->db->from($view_name);
        $this->db->where($where_params);
        if (!empty($like_params)) {
            foreach ($like_params as $field => $value) {
                $this->db->like($field, $value);
            }
        }
        $this->db->order_by('ID', "desc");
        if(isset($config['limit']))
        {
          $this->db->limit($config['limit']);
        }
        return $this->db->get()->result();

    }

    public function CreateSubCategory($value)
    {
        $procedure = callProcedure("PrcCreateSubCategory", 1);
        $result = $this->db->query($procedure, array('category_value' => $value));
        if ($result !== null) {
            return $result->result();
        }
        return false;
    }

    public function CreateBusinessField($data = array())
    {
        if (count($data) > 0) {
            $procedure = callProcedure('PrcCreateBusinessField', count($data));
            $query = $this->db->query($procedure, $data);
            if ($query !== null) {
                return true;
            }
        }
        return false;
    }
}
