<?php 
    class Customer_Model extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }

        function searchForTag($tag){
            $this->db->select('*');
            $this->db->from('vw_listcustomer');
            $this->db->like('Tag', $tag);
    
            $query = $this->db->get()->result();
            if(empty($query))
            {
                return false;
            }
            return $query;
        }
    }