<?php 
    class Employee_Model extends CI_Model
    {
        public $table = null;
        function __construct()
        {
            parent::__construct();
            $this->table = "employee";
        }

        function FindOfEmployee($username)
        {
            return $this->db->where(array("Username" => $username, "IsActive" => 1))->get($this->table)->num_rows();
        }

        function Login($username, $password)
        {
            return $this->db->where(array("username" => $username, "password" => $password, "IsActive" => 1))->get($this->table)->row();
        }

        function Employees()
        {
            return $this->db->select("*")->from($this->table)->get()->result();
        }
    }