<?php $this->load->view('Include/Header')?>
<?php $this->load->view('Components/Common/Navbar_Component');?>
<div class="container-fluid" id="showAppointment">
    <div class="row">
        <?php if ($AppointmentID > 0): ?>

        <div class="col-12 bg-primary text-white p-2">
            <h4>
                <i class="fas fa-hashtag"></i> <?=$appointmentData->ID?> -
                <?=$appointmentData->Fullname?> Adlı Müşterinin Randevu Bilgisi
            </h4>
            <hr class="bg-white">

            <strong>Tarih:</strong>
            <?=$appointmentData->FullDate?>
            <?php echo ($appointmentData->AppointmentTime < date('H:i:s') && $appointmentData->State == 0) ? '<br>  <strong style="color:#90e840;"><i class="fas fa-exclamation"></i> Randevunun Saati Geçti! </strong>' : "" ?>
            <br>
            <strong>Durum: </strong> <?=$appointmentData->StateOfDescription?>
            <br>
            <strong>Görevli Personel: </strong> <?=$appointmentData->EmployeeName?>
            <br>
            <strong>Yapılan İşlem: </strong> <?=$appointmentData->JobField?>
            <hr>
            <h4>Müşteri Bilgisi</h4>
            <strong> <a href="tel:" class="text-white"><i class="fas fa-phone"></i> <?=$appointmentData->Phone?></a> </strong>-
            <strong> <i class="fas fa-user"></i> <?=$appointmentData->Fullname?> </strong>
            <hr>
            <h4> İşlemler </h4>
            <div class="list-group">
                <a href="" class="list-group-item list-group-item-action"><i class="fas fa-file"></i> Müşteri Detayı </a>
                <a href="" class="list-group-item list-group-item-action"><i class="fas fa-phone"></i> Müşteriyi Ara </a>
                <?php if ($appointmentData->State != 0): ?>
                <button @click="repeatAppointmentShow = !repeatAppointmentShow;" class="list-group-item list-group-item-action"><i class="fas fa-redo"></i> Randevuyu Tekrarla</button>
                <div v-if="repeatAppointmentShow" class="col-12 p-2 border-left border-right sshadow-lg">
                    <strong>Not: İşlemler aynı kalmaktadır, sadece randevu tarihi ve saatini değiştirerek aynı bilgiler ile randevuyu tekrarlayabilirsiniz.</strong>
                    <br>
                    <label> Randevu Tarihi </label>
                    <input type="date" class="form-control"
                     min="<?=date("Y-m-d")?>"
                     v-model="appointmentDate">
                    <label for="">Randevu Saati </label>
                    <input class="form-control" v-model="appointmentTime" type="time">
                    <div class="col-12 p-2 hide my-2 repeat_loader bg-dark text-white text-center rounded shadow-lg">
                        İşlem Yapılıyor Bekleyin.. <i class="fas fa-spinner fa-spin"></i>
                    </div>
                    <button @click="repeatAppointment(<?=$appointmentData->ID?>)" type="button" class="btn btn-primary form-control my-2">
                        Randevuyu Tekrarla <i class="fas fa-redo"></i>
                    </button>
                </div>
                <?php else: ?>
                <button @click="appointmentCancelVisible()" class="list-group-item list-group-item-action"><i class="fas fa-trash"></i> Randevuyu İptal Et </button>
                <div v-if="appointmentCancelShow == true" class="text-black list-group-item list-group-item-action bg-secondary text-dark">
                    <label> İptal Nedeni </label>
                    <input type="text" class="form-control" v-model="appTransactionData.StateText" />
                    {{appTransactionData.StateText}}
                    <br>
                    <button @click="truncateStateAppointment(<?=$appointmentData->ID?>, 3)" type="button" class="btn btn-sm btn-danger"> Randevuyu İptal Et </button>
                </div>
                <button  @click="truncateStateAppointment(<?=$appointmentData->ID?>, 2)" class="list-group-item list-group-item-action"><i class="fas fa-ban"></i> Müşteri Randevuya Gelmedi</button>
                <button  @click="truncateStateAppointment(<?=$appointmentData->ID?>, 1)" class="list-group-item list-group-item-action"><i class="fas fa-check"></i> Müşteri Randevuya Geldi</button>
                <?php endif;?>
                <div class="col-12 p-2 hide loader bg-dark text-white text-center list-group-item list-group-item-action">
                    İşlem Yapılıyor Bekleyin.. <i class="fas fa-spinner fa-spin"></i>
                </div>
            </div>
            <?php if ($appointmentData->State != 0): ?>
            <div class="shadow-lg p-2 border rounded-bottom">
                <h4>Randevu Detayı</h4>
                <strong>
                    <i class="fas fa-edit"></i> Açıklama: <?=$appState->DetailText?>
                </strong>
                <br>
                <strong><i class="fas fa-calendar"></i> İşlem Tarihi: <?=$appState->Fulldate?> </strong>
                <br>
                <strong><i class="fas fa-bolt"></i> İşlem Yapan Personel: <?=$appState->Fullname?></strong>
            </div>
            <?php endif;?>
        </div>

        <?php else: ?>
        <div class="my-2 p-2 col-12">
            <div class="alert alert-danger">
                <strong> Randevuya Ait Bir Veri Bulunanamadı! </strong>
                <hr>
                <p>
                    Bu kimlik numarasına göre bir randevu bulunamadı!
                </p>
                <a href="<?php echo base_url('Randevu/Olustur'); ?>" class="btn btn-sm btn-info">
                    Randevu almak için tıklayın!
                </a>
            </div>
        </div>
        <?php endif;?>
    </div>
</div>
<script src="<?=base_url("Assets/assets/appointment.js?v" . rand(000, 10000))?>"></script>
<?php $this->load->view('Include/Footer');?>