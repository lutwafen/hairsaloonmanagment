<?php $this->load->view('Include/Header')?>
<?php $this->load->view('Components/Common/Navbar_Component');?>


<div class="col-12">
    <strong>Randevu Oluştur</strong>
    <hr>
    <form action="<?=base_url("Appointment/CreateAppointment")?>" method="post">
        <div class="form-group">
            <label>Randevu Tarihi</label>
            <input
            type="date"
            class="form-control"
            value="<?=date("Y-m-d")?>"
            name="appointment_date"
            min="<?=date("Y-m-d")?>">
        </div>
        <div class="form-group">
            <label>Randevu Saati</label>
            <input
            type="time"
            class="form-control"
            value="<?=date("H:i:s")?>"
            name="appointment_time"
            >
        </div>
        <div class="form-group">
            <label>Müşteri Adı Soyadını Yazınız</label>
            <input
            type="text"
            class="form-control txtCustomerTag"
            placeholder="Müşteri Adı ve Soyadını Yazınız">
        </div>
        <div class="form-group list-group searchCustomerList my-2"></div>

        <div class="form-group">
            <label> Personel Seçimi </label>
            <br>
            <small> Otomatik Olarak Seçilen Personel Sizsiniz </small>
            <select class="form-control" name="employee_id">
                <option value="0"> Lütfen Bir Personel Seçiniz </option>
                <?php foreach ($EmployeeList as $list) {?>
                    <option <?=$list->ID == $this->session->EmployeeID ? "selected" : ""?> value="<?=$list->ID?>">
                        <?=$list->Fullname?>
                    </option>
                <?php }?>
            </select>
        </div>

        <div class="form-group">
            <label for="">İş Kategorisi Seçiniz</label>
            <select class="form-control businessTopCategorySelect">
                <option value="-1"> Lütfen Bir Seçim Yapınız</option>
                <option value="0"> Kategorisiz </option>
                <?php foreach ($TopBusinessCategory as $category) {?>
                    <option value="<?=$category->TopCategoryID?>"><?=$category->TopCategoryValue?></option>
                <?php }?>
            </select>
        </div>

        <div class="form-group listBusiness hide"></div>
        <div class="form-group selectedBusiness border p-2 bg-primary text-white hide">
            <strong>Seçilen İş Birimi: Test <i class="fas fa-random"></i> Test</strong>
        </div>
        <!-- Hidden Inputs -->
        <input type="hidden" name="customer_id" value="0" class="customer_id">
        <input type="hidden" name="business_id" value="0" class="business_id">
        <!-- Hidden Inputs -->

        <button type="sumbit" class="btn btn-block btn-secondary">
            Randevuyu Oluştur
        </button>
    </form>
</div>


<script src="<?=base_url("Assets/assets/panel.js?v" . rand(000, 10000))?>"></script>
<?php $this->load->view('Include/Footer');?>