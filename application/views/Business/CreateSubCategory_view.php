<?php
$this->load->view('Include/Header');
$this->load->view('Components/Common/Navbar_Component');
?>

<div class="container-fluid my-2">
    <div class="row">
        <div class="col-12">
            <h3> <?=$Business->TopCategoryValue?> </h3> Ait bir alt işlem eklenecek!
            <hr>
        </div>

        <div class="col-12">
            <small style="color:red;"> Lütfen Aşağıdaki Alanları Eksiksiz Doldurunuz! </small>
        </div>

        <div class="col-12">
            <?php $this->load->view('Components/Business/CreateSubCategory_Component', $Business);?>
        </div>
    </div>
</div>
<script src="<?=base_url('Assets/assets/business.js?v=' . rand(0, 10))?>"></script>
<?php $this->load->view('Include/Footer');?>