<!-- Business Select -->
<div class="border col-12" id="business_info">
    <label> Bilgi Almak İstediğiniz İş Kategorisini Seçiniz </label>
    <select class="form-control selectTopCategory">
        <option value="0"> Lütfen Bir İş Tanımı Seçiniz</option>
        <?php foreach ($BusinessList as $list): ?>
        <option value="<?=$list->ID?>"> <?=$list->Value?></option>
        <?php endforeach;?>
    </select>
    <div class="business_field_loader hide text-center my-2 border p-2 bg-primary text-white">
        <i class="fas fa-spinner fa-spin"></i> Veriler Yükleniyor
    </div>

    <div class="selectedBusiness"></div>
</div>

<!-- Business Select -->