
<div class="border p-2 my-2 mx-auto shadow-lg">
    <div class="availableCategory">
        Dahil Edilecek Kategori: <?=$Business->TopCategoryValue?>
        <label> Dahil Edilecek İşlem Adını Yazınız. Eğer bu birim yok ise oluşturabilirsiniz.</label>
        <input type="text" class="form-control getAvailableSubCategory">

        <div class="list-group list-get-category">
        </div>
        <div class="secondStep hide">
            <input type="hidden" class="selectedSubCategoryID" value="0">
            <input type="hidden" class="TopCategoryID" value="<?=$Business->TopCategoryID?>" />
            <strong class="selectedSubCategoryValue"> Seçilen Birim: </strong>
            <label for="">Ücretlendirmeyi Belirleyin</label>
            <input type="number" class="form-control price_value" step="any">

            <?php if ($this->session->IsAdmin): ?>
            <label> Prim Oranı (% Cinsinden, 0 ve 100 Arası Tam Sayı Değerleri Alabilir) </label>
            <input min="0" max="100" type="number" class="form-control bonus_ratio">
            <?php endif;?>
            <button type="button" class="btnCreateField my-2 btn btn-block btn-danger">
                Oluştur
            </button>
        </div>

        <div
        style="font-size:16px;"
        class="bg-danger text-white hide border shadow-lg p-2 my-2 text-center create_sub_category_loader">
            <i class="fas fa-spinner fa-spin "></i> Oluşturuluyor Lütfen Bekleyin!
        </div>

    </div>
</div>
