<div id="edit_modal" class="modal">
  <div class="modal-dialog" role="document">
    <div  id="app" class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?=$businessData->Description?> Adlı İş Alanı Düzenlemesi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body w-100 p-2">

        <!-- Hidden Input -->
        <input type="hidden" class="sub_category_id" value="<?=$businessData->SubCategoryID?>">
        <input type="hidden" class="top_category_id" value="<?=$businessData->TopCategoryID?>">
        <!-- Hidden Input -->

        <label> İşlem Adı </label>
        <input type="text" class="form-control sub_category_name" value="<?=$businessData->SubCategoryValue?>">
        <label> Ücret </label>
        <input type="number" v-model="price" class="form-control price_value" step="any" value="<?=$businessData->Pricing?>">
        <label> Prim Oranı (% Cinsinden ve 0-100 Arası bir veri giriniz.)</label>
        <input type="number" v-model="bonus_ratio" class="form-control bonus_ratio" min="0" max="100" value="<?=$businessData->BonusRatio?>">


        <div class="row bonus_calculate">

            <div class="col-12" v-if="price != const_price">
              Fiyat Değişikliğinden Sonra Aradaki Fark: <strong>{{changePrice}} ₺</strong><hr>
            </div>

            <div class="col-6">Prim Tutarı</div>
            <div class="col-6 text-right bonus_amount">{{bonusRatio}}</div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btnEditBusinessComplete" @click="EditBusinessField()">Güncelle <i class="fas fa-edit"></i></button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">İptal Et <i class="fas fa-ban"></i></button>
      </div>
    </div>
  </div>
</div>

<script>
new Vue({
  el: "#app",
  data: {
    bonus_ratio: <?=$businessData->BonusRatio?>,
    price : <?=$businessData->Pricing?>,
    const_price : <?=$businessData->Pricing?>,
    business_id : <?=$businessData->ID?>,
    bonus_amount : 0
  },
  created : function(){
    var result = parseFloat(this.price * (this.bonus_ratio / 100));
    this.bonus_amount = result.toFixed(2);
  },
  methods: {
    EditBusinessField: function(){
      var formData = new URLSearchParams();
      formData.append('business_id', this.business_id);
      formData.append('pricing', this.price);
      formData.append('bonus_ratio', this.bonus_ratio);

      axios.post(config.base + 'Business/EditBusinessField', formData).then(function(response){
        if(response.data == "ok")
        {
          alert("Düzenleme Başarılı, Yönlendiriliyor Lütfen Bekleyin!");
        }
        else{
          alert("Bir Sorun Oldu, Güncelleştirme Yapılamadı! Lütfen Daha Sonra Tekrar Deneyin!");
        }

        window.location.href="";
      })
      .catch(function(err){
        console.log(err);
        alert("İş Birimi Düzenlenirken Bir Hata Oluştu!");
      });
    }
  },
  computed: {
    bonusRatio: function(){
      var result = parseFloat(this.price * (this.bonus_ratio / 100));
      this.bonus_amount = result.toFixed(2);
      return this.bonus_amount;
    },
    changePrice: function(){
      var result = parseFloat(this.const_price - this.price).toFixed(2);

      if(result < 0)
      {
        result = " + " + (result * -1);
      }
      else{
        result *=  -1 ;
      }

      return result;
    }
  }
});
</script>
