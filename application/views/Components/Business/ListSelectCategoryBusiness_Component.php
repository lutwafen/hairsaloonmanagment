<?php if (IsAdmin()): ?>
<a href="<?=base_url('Kategori/Alt/Olustur/' . $TopCategoryID)?>" class="btn btn-light btn-sm my-2 p-2">
    <i class="fas fa-folder-plus"></i> Bu kategoriye ait bir işlem oluştur
</a>
<?php endif;?>



<?php if (count($BusinessList) > 0): ?>
    <?php if (count($BusinessList) > 5): ?>
    <div>
        <strong>Son 5 Adet İş Birimi Gösterilmektedir! <a href="<?=base_url('Business/ShowBusinessField/' . $BusinessList[0]->TopCategoryID)?>">Tümünü Görmek İçin Tıklayın!</a></strong>
    </div>
    <?php endif;?>
    <?php foreach ($BusinessList as $list): ?>
    <div class="border my-2 p-2">
        <h3> <?=$list->SubCategoryValue?>  </h3>
        <span style="font-size:14px;"><?=$list->Description?></span>
        <hr>
        <div class="row">
            <div class="col-6">Fiyat </div>
            <div class="col-6 text-right"><?=$list->Pricing > 0 ? $list->Pricing . ' ₺' : '<strong style="color:red"> Manuel Olarak Belirlenecek </strong>'?></div>
            <div class="col-6">Kategori </div>
            <div class="col-6 text-right"><?=$list->TopCategoryValue?></div>

            <?php if (IsAdmin()): ?>
            <div class="col-6">Prim Oranı </div>
            <div class="col-6 text-right"><?=$list->BonusRatio?> % </div>

            <div class="col-6">Prim Tutarı </div>
            <div class="col-6 text-right"><?=$list->Pricing * ($list->BonusRatio / 100)?> ₺ </div>

            <div class="col-12">
                <hr>
            </div>
            <div class="col-8 mx-auto">
                <button class="btn-block btn btn-sm btn-primary btnEditBusinessField" data-business-id="<?=$list->ID?>">Düzenle <i class="fas fa-edit"></i></button>
            </div>
            <div class="col-8 mx-auto">

                <button
                class="btn-block btn btn-sm <?=$list->IsActive == 1 ? 'btn-danger' : 'btn-warning'?> btnChangeBusinessActive"
                data-business-id="<?=$list->ID?>"
                data-business-top-category="<?=$list->TopCategoryValue?>"
                data-business-sub-category="<?=$list->SubCategoryValue?>"
                data-change-value="<?=$list->IsActive == 1 ? 0 : 1?>"
                >
                   <?php if ($list->IsActive == 1): ?> Kapat <i class="fas fa-trash"></i>
                    <?php else: ?> Aç <i class="fas fa-lock-open"></i>
                    <?php endif;?>
                </button>

            </div>
            <?php else: ?>
                <div class="col-12 text-center">
                    <strong> İşlem Birimlerinde Düzenleme Yapabilmek İçin Lütfen Yönetici Girişi Yapınız! </strong>
                </div>
            <?php endif;?>
        </div>
    </div>
    <?php endforeach;?>

    <div class="modal_field"></div>

<?php else: ?>

<div class="col-12 my-2">
    <div class="alert alert-warning">
        <h3> Bu kategorinin herhangi bir işlem birimi yok! </h3>
        <a href="<?=base_url('Kategori/Alt/Olustur/' . $TopCategoryID)?>" class="btn btn-light btn-sm my-2 p-2">
            <i class="fas fa-folder-plus"></i> Bu kategoriye ait bir birim ekle
        </a>
    </div>
</div>

<?php endif;?>