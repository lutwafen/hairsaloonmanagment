<!-- Start: Navbar -->
<div>
    <!-- Start: Navbar Top -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center align-self-center" style="background-color: #574b88;"><a href="<?=base_url()?>"><strong
                    style="font-size: 20px;color: rgb(255,255,255);">Kuafor Uygulaması</strong></a></div>
            <div class="col-12 align-self-center" style="padding: 0px;"><button
                    class="btn btn-secondary btn-block btn-sm border rounded-0 shadow btnLaunchNavbar"
                    type="button"><i class="la la-bars" style="font-size: 16px;"></i></button></div>
        </div>
    </div>
    <!-- End: Navbar Top -->
    <!-- Start: Navbar Section -->
    <div class="container-fluid" id="navbar-section" style="display: none;">
        <div class="row">
            <div class="col-12 align-self-center" style="padding: 0px;">
                <div class="list-group shadow">
                    <!-- Start: Profile -->
                    <div class="list-group-item list-group-item-info">
                        <div class="row">
                            <div class="col-12"><a href="#"><strong>@<?=$this->session->Employee->Username?></strong></a>
                                <hr>
                            </div>
                            <div class="col-12">
                                <a href="<?=base_url("Login/Logout")?>" class="btn btn-outline-primary btn-block" role="button">
                                    cikis yap
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- End: Profile -->
                    <!-- Start: Events -->
                    <div class="list-group-item list-group-item-info">
                        <div class="row">
                            <div class="col-8 text-left">
                                <h4>Olaylar</h4>
                            </div>
                            <div class="col-4 text-center"><button class="btn btn-primary btn-block btnEventShow"
                                    type="button"><i class="la la-eye"></i></button></div>
                            <div class="col-12">
                                <hr>
                            </div>
                            <!-- Start: Appointment_Event -->
                            <div class="col-12 event_content event_appointment" style="display:none;">

                            </div>
                            <!-- End: Appointment_Event -->
                            <!-- Start: Business_Event -->
                            <div class="col-12 event_content" style="display:none;">
                                <hr>
                            </div>
                            <!-- End: Business_Event -->
                        </div>
                    </div>
                    <!-- End: Events -->
                    <a href="" class="text-center bg-light list-group-item list-group-item-info">Randevu İşlemleri</a>
                    <a href="<?=base_url('Is')?>" class="text-center bg-light list-group-item list-group-item-info">İş Alanları</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End: Navbar Section -->
</div>
<!-- End: Navbar -->

<script src="<?=base_url('Assets/assets/navbar.js?v=' . rand(0, 10))?>"></script>