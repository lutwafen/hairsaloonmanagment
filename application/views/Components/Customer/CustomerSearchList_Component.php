<a 
class="list-group-item list-group-item-action disabled" 
style="background-color:#36b345; color:white;"> 
    <strong> Müşteri Seçimi </strong>
</a>

<?php foreach($searchResult as $row){ ?>
    <a 
    data-customer-id="<?=$row->ID?>" 
    data-customer-name="<?=$row->Fullname?>" 
    class="list-group-item list-group-item-action list-selected-customer"> 
        <div class="row">
            <div class="col-3">
                <i class="fas fa-user"></i>  
            </div>
            <div class="col-9 text-right">
                <?=$row->Fullname?>
            </div>
            <div class="col-3">
                <i class="fas fa-phone"></i> 
            </div>
            <div class="col-9 text-right">
             <?=$row->Phone?>
            </div>
            <div class="col-12 text-center">
                <small>Seçmek İçin Üzerine Tıklayın!</small>
            </div>
        </div>
    </a>
<?php } ?>