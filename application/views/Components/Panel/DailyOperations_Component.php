
<div class="container-fluid m-0 border  DailyOperationsArea " id="showAppointment">
    <div class="row">

        <!-- Header -->
        <div class="col-12">
            <strong>Bugüne Ait Randevular</strong>
            <hr class="bg-light">
        </div>
        <!-- Header -->

        <!-- Operation Filter Area -->
        <div class="col-12 bg-dark hide">
            KODLANACAK
        </div>
        <!-- Operation Filter Area -->

        <!-- Content -->

        <div class="col-12">
            <?php foreach ($AppointmentList as $list): ?>
                <div class="card bg-dark text-white p-2 my-2 shadow-lg">
                    <div class="card-body">
                        <h5 class="card-title">#<?=$list->ID?> - <?=$list->Fullname?></h5>
                        <h6 class="card-subtitle mb-2 text-muted"><?=$list->FullDate?></h6>
                        <p class="card-text">
                            <?php if ($list->AppointmentTime < date('H:i:s')): ?>
                                <i class="fas fa-exclamation"></i> Randevu Zamanı Geçmiş
                                <button @click="truncateStateAppointment(<?=$list->ID?>, 2)" class="btn btn-warning btn-sm"><i class="fas fa-ban"></i> Gelmedi Olarak İşaretle</button>
                                <br>
                            <?php endif;?>
                            <i class="fas fa-tasks"></i> <?=$list->StateOfDescription?>
                            <br>
                            <i class="fas fa-cog"></i> <?=$list->JobField?>
                        </p>
                        <button @click="truncateStateAppointment(<?=$list->ID?>, 1)" class="btn btn-light btn-sm">
                            <i class="fas fa-check"></i> Geldi
                        </button>
                        <a href="<?=base_url('Appointment/ShowAppointment/' . $list->ID)?>" class="btn btn-danger btn-sm">
                            <i class="fas fa-list"></i>  Detay
                        </a>

                        <div class="col-12 p-2  loader bg-light text-dark border rounded hide text-center my-2">
                            İşlem Yapılıyor Bekleyin.. <i class="fas fa-spinner fa-spin"></i>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>

        <!-- Content -->

    </div>
</div>

<script src="<?=base_url("Assets/assets/components/daily_operations_com.js?v=" . rand(0, 19))?>"></script>
<script src="<?=base_url("Assets/assets/appointment.js?v=" . rand(0, 19))?>"></script>