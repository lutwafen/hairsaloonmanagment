<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Kuafor App</title>
    <link rel="stylesheet" href="<?php echo base_url("Assets/assets/") ?>bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,400italic"> -->
    <link rel="stylesheet" href="<?php echo base_url("Assets/assets/") ?>fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?php echo base_url("Assets/assets/") ?>fonts/line-awesome.min.css?v=<?=rand(0, 100)?>">
    <link rel="stylesheet" href="<?php echo base_url("Assets/assets/") ?>common.css?v=<?=rand(0, 10000)?>">

    <script src="<?=base_url()?>/Assets/assets/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>

<script>
  var config = {
    "base" : "<?=base_url()?>"
  };


</script>

<script src="<?php echo base_url("Assets/assets/") ?>js/jquery.min.js"></script>
<script src="<?php echo base_url("Assets/assets/") ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url("Assets/assets/") ?>js/script.js?v=<?=rand(0, 10000)?>"></script>
<script src="<?php echo base_url('Assets/assets/') ?>sweetalert2_9.js"></script>
