<p style="border-bottom: 1px solid red;">Yaklaşan Randevunuz Var&nbsp;<br></p>
<p>
    Tarih: <?=$appointment_data->AppointmentDate?> - <?=$appointment_data->AppointmentTime?><br>
    Müşteri Adı Soyadı: <?=$appointment_data->Fullname?><br>
    İşlem: <?=$appointment_data->JobField?><br>
</p>
<div class="btn-group" style="width: 100%;">
    <button class="btn btn-primary"
        type="button" style="width: 100%;">
        islem secenekleri
    </button>
    <button
        class="btn btn-primary dropdown-toggle dropdown-toggle-split"
        data-toggle="dropdown" aria-expanded="false" type="button">
    </button>
    <div class="dropdown-menu" role="menu">
        <a class="dropdown-item"
            role="presentation" href="#">
            Müşteri Randevuya Geldi
        </a>
        <a class="dropdown-item" role="presentation" href="#">
            Randevu İptal Edildi
        </a>
        <a class="dropdown-item" role="presentation"
            href="#">
            Randevuya Gelmedi
        </a>
        <a class="dropdown-item bg-primary text-white" role="presentation"
            href="tel:<?=$appointment_data->Phone?><br>">
            <i style="font-size:1em" class="fas fa-phone-volume"></i> Müşteriyi Ara 
        </a>
    </div>
</div>