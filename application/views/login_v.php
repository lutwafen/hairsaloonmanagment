
<div class="container-fluid">
    <div class="row">
        <!-- Giriş Formu -->
        <div class="col-12">
            <strong> Kullanıcı Adınızı Giriniz </strong>
            <input type="text" class="form-control l_username">
            <strong> Parolanızı Giriniz </strong>
            <input type="password" class="form-control l_password">
            <button type="button" class="my-2 btn btn-primary btn-block btn-outline btnLogin">
                Giriş Yap
            </button>

            <div class="hide alert alert-danger login_err"></div>
        </div>
        <!-- Giriş Formu -->

        <!-- Önceden Giriş Yapılan Alanlar -->
        <div class="col-12 lastLoginData">
            <h4> Geçmiş Giriş Hareketleri </h4>
            <hr>
            <small>Giriş Yapmak İstediğiniz Hesabı Seçin</small>
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action lastLoginItem">
                    @HesapAdi
                </a>
                <a href="#" class=" text-white hide bg-primary list-group-item list-group-item-action item-content">
                    <strong>Bu Hesaba Ait Şifre</strong>
                    <input type="text" class="form-control my-2 lastLoginPassword">
                    <button data-account="@HesapAdi" type="button" class="btnLastLogin btn-block btn-light btn">
                        Giriş Yap
                    </button>
                </a>
                <a href="#" data-account="HesapAdi" class="hide account-delete text-muted  bg-secondary list-group-item list-group-item-action item-content">
                    Hesabı Sil <i style="font-size:16px" class="la la-trash"></i>
                </a>
            </div>
        </div>
        <!-- Önceden Giriş Yapılan Alanlar -->

    </div>
</div>

<script src="<?=base_url('Assets/assets/login.js?v='.rand(0,10000))?>"></script>