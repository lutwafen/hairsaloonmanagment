<?php $this->load->view('Include/Header');?>
<?php $this->load->view('Components/Common/Navbar_Component');?>

<!-- Fast Access Navigation -->
<div class="container-fluid bg-dark text-white">
    <div class="row">
        <div class="col-12 p-2">
            <span>Hızlı Erişim</span>
            <select class="form-control fastAccessSelect">
                <option value="0">Lütfen Bir İşlem Seçiniz</option>
                <optgroup label="Randevu İşlemleri">
                    <option value="randevu_1">Randevu Al</option>
                    <option value="randevu_2">Randevu Kaydını Göster</option>
                </optgroup>
                <optgroup label="Müşteri İşlemleri"></optgroup>
                <?php if ($this->session->IsAdmin): ?>
                <optgroup label="Personel İşlemleri [ADMIN]"></optgroup>
                <?php endif;?>
            </select>
        </div>
    </div>
</div>
<!-- Fast Access Navigation -->

<!-- Fast Access Content -->
<div class="container-fluid border shadow-lg">
    <div class="row fastAccessContent p-2"></div>
</div>
<!-- Fast Access Content -->

<script src="<?=base_url("Assets/assets/panel.js?v=" . rand(0, 10000))?>"></script>
<?php $this->load->view('Include/Footer');?>