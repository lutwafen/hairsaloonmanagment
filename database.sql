
CREATE TABLE Appointment(
	`ID` int NOT NULL,
	`AppointmentDate` date NULL,
	`AppointmentTime` varchar(5) NULL,
	`CustomerID` int NULL,
	`EmployeeID` int NULL,
	`BusinessID` int NULL,
	`State` tinyint Unsigned NULL,
 CONSTRAINT `PK_Appointment` PRIMARY KEY 
(
	`ID` ASC
) 
);

/* SET ANSI_PADDING OFF */
 
/****** Object:  Table [dbo].[BusinessTransaction]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE BusinessTransaction(
	`ID` int NOT NULL,
	`TopCategoryID` int NULL,
	`SubCategoryID` int NULL,
	`Pricing` Decimal(15,2) NULL,
	`Description` varchar(120) NULL,
	`NonCategory` Tinyint NULL,
 CONSTRAINT `PK_BusinessTransaction` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[Customer]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE Customer(
	`ID` int AUTO_INCREMENT NOT NULL,
	`Fullname` varchar(32) NULL,
	`Phone` varchar(16) NULL,
	`Tag` varchar(20) NULL,
 CONSTRAINT `PK_Customer` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[CustomerSafeData]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE CustomerSafeData(
	`ID` int NOT NULL,
	`CustomerID` int NULL,
	`TotalPrice` Decimal(15,2) NULL,
	`SafeID` int NULL,
 CONSTRAINT `PK_CustomerSafeData` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[CustomerSafeMovementInfo]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE CustomerSafeMovementInfo(
	`ID` int AUTO_INCREMENT NOT NULL,
	`CustomerID` int NULL,
	`TransactionID` int NULL,
	`TransactionPrice` Decimal(15,2) NULL,
	`EmployeeID` int NULL,
	`SafeID` int NULL,
	`SafeMovementID` int NULL,
	`CreatedAt` datetime(3) NULL,
 CONSTRAINT `PK_CustomerSafeMovementInfo` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[Employee]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE Employee(
	`ID` int NOT NULL,
	`Fullname` varchar(50) NULL,
	`Username` varchar(16) NULL,
	`Password` varchar(16) NULL,
	`Address` varchar(100) NULL,
	`Phone` varchar(16) NULL,
	`IsActive` Tinyint NULL,
	`IsAdmin` Tinyint DEFAULT 0,
 CONSTRAINT `PK_Employee` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[EmployeeLog]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE EmployeeLog(
	`ID` int NOT NULL,
	`EmployeeID` int NULL,
	`LogReg` text NULL,
	`LogDate` datetime(3) NULL,
 CONSTRAINT `PK_EmployeeLog` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[Group]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
/* SET ANSI_PADDING ON */
 
CREATE TABLE Groups(
	`ID` int NOT NULL,
	`CreatedCustomerID` int NULL,
	`SafeID` int NULL,
	`NumberOfPeople` int NULL,
	`Date` datetime(3) NULL,
	`Tag` varchar(50) NULL,
 CONSTRAINT `PK_Group` PRIMARY KEY 
(
	`ID` ASC
) 
);

/* SET ANSI_PADDING OFF */
 
/****** Object:  Table [dbo].[GroupCustomer]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE GroupCustomer(
	`ID` int NOT NULL,
	`CustomerID` int NULL,
	`GroupID` int NULL,
	`MovementCount` int NULL,
	`PricingStatus` Tinyint NULL,
 CONSTRAINT `PK_GroupCustomer` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[GroupSafeMovement]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE GroupSafeMovement(
	`ID` int NOT NULL,
	`SafeMovementID` int NULL,
	`SafeID` int NULL,
	`GCustomerID` int NULL,
	`GroupID` int NULL,
 CONSTRAINT `PK_GroupSafeMovement` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[Material]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
/* SET ANSI_PADDING ON */
 
CREATE TABLE Material(
	`ID` int NOT NULL,
	`Name` varchar(50) NULL,
	`CategoryID` int NULL,
	`Price` Decimal(15,2) NULL,
	`UsePrice` Decimal(15,2) NULL,
	`Rate` Double NULL,
	`Count` int NULL,
 CONSTRAINT `PK_Material` PRIMARY KEY 
(
	`ID` ASC
) 
);

/* SET ANSI_PADDING OFF */
 
/****** Object:  Table [dbo].[MaterialCategory]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE MaterialCategory(
	`ID` int NOT NULL,
	`Value` varchar(50) NULL,
	`Tag` varchar(50) NULL,
 CONSTRAINT `PK_MaterialCategory` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[MaterialUseData]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE MaterialUseData(
	`ID` int NOT NULL,
	`CustomerID` int NULL,
	`MaterialID` int NULL,
	`EmployeeID` int NULL,
	`SafeID` int NULL,
	`SafeMovementID` int NULL,
	`UseDate` datetime(3) NULL,
	`UseInfo` varchar(100) NULL,
	`UsePrice` Decimal(15,2) NULL,
 CONSTRAINT `PK_MaterialUseData` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[Payment]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE Payment(
	`ID` int NOT NULL,
	`MethodID` int NULL,
	`SafeID` int NULL,
	`CustomerID` int NULL,
	`EmployeeID` int NULL,
	`Discount` Decimal(15,2) NULL,
	`TotalPrice` Decimal(15,2) NULL,
	`CashValue` Decimal(15,2) NULL,
	`CardValue` Decimal(15,2) NULL,
	`Description` varchar(50) NULL,
 CONSTRAINT `PK_Payment` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[PaymentMethod]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE PaymentMethod(
	`ID` int NOT NULL,
	`Value` varchar(50) NULL,
	`Description` varchar(50) NULL,
 CONSTRAINT `PK_PaymentMethod` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[Safe]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE Safe(
	`ID` int NOT NULL,
	`TotalPrice` Decimal(15,2) NULL,
	`TotalMaterialPrice` Decimal(15,2) NULL,
	`TotalMovementPrice` Decimal(15,2) NULL,
	`MovementCount` int NULL,
	`CustomerID` int NULL,
	`PaymentStatus` Tinyint NULL,
	`IsOpen` Tinyint NULL,
	`CreatedDate` datetime(3) NULL,
	`CreatedEmployeeID` int NULL,
	`Tag` varchar(50) NULL,
 CONSTRAINT `PK_Safe` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[SafeMovement]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE SafeMovement(
	`ID` int NOT NULL,
	`SafeID` int NULL,
	`EmployeeID` int NULL,
	`TransactionID` int NULL,
	`TransactionText` varchar(50) NULL,
	`CreatedDate` datetime(3) NULL,
	`Price` Decimal(15,2) NULL,
	`IsFinish` Tinyint NULL,
 CONSTRAINT `PK_SafeMovement` PRIMARY KEY 
(
	`ID` ASC
) 
);

/****** Object:  Table [dbo].[TransactionCategory]    Script Date: 15.11.2019 05:12:34 ******/
/* SET ANSI_NULLS ON */
 
/* SET QUOTED_IDENTIFIER ON */
 
CREATE TABLE TransactionCategory(
	`ID` int NOT NULL,
	`Value` varchar(50) NULL,
	`Description` varchar(50) NULL,
 CONSTRAINT `PK_TransactionCategory` PRIMARY KEY 
(
	`ID` ASC
) 
);
